#ifndef DATAWIDGET_H
#define DATAWIDGET_H

#include <array>

#include <QWidget>

class QTabWidget;
class OptionsPrintWidget;
class OptionsPlasticFeedWidget;
class OptionsFiberFeedWidget;
class OptionsFilamentWidget;
class OptionsReinforcingFiberWidget;

class Options;

class DataWidget : public QWidget
{
  Q_OBJECT
public:
  explicit DataWidget(QWidget *parent = nullptr);
  void readFrom(const Options &options);
  void writeTo(Options *options) const;

signals:

private:
  QTabWidget *optionTabs_;
  QTabWidget *ptabPlasticFeed_;

  OptionsPrintWidget *pwgtPrint_;
  std::array<OptionsPlasticFeedWidget *, 2> pwgtPlasticFeedNozzle_;
  OptionsFiberFeedWidget *pwgtFiberFeed_;
  OptionsFilamentWidget *pwgtFilament_;
  OptionsReinforcingFiberWidget *pwgtReinforcingFiber_;

  void reTranslateUi();

};

#endif // DATAWIDGET_H
