#include "DataWidget.h"

#include <QVBoxLayout>
#include <QTabWidget>

#include "widgets/OptionsPrintWidget.h"
#include "widgets/OptionsPlasticFeedWidget.h"
#include "widgets/OptionsFiberFeedWidget.h"
#include "widgets/OptionsFilamentWidget.h"
#include "widgets/OptionsReinforcingFiberWidget.h"
#include "types/Config.h"

DataWidget::DataWidget(QWidget *parent) : QWidget(parent)
{
  QVBoxLayout *plytTop = new QVBoxLayout(this);
  {
    optionTabs_ = new QTabWidget(this);
    pwgtPrint_ = new OptionsPrintWidget(optionTabs_);
    optionTabs_->addTab(pwgtPrint_, "Print");

    ptabPlasticFeed_ = new QTabWidget(this);
    for (size_t i = 0; i < pwgtPlasticFeedNozzle_.size(); ++i)
    {
      pwgtPlasticFeedNozzle_[i] = new OptionsPlasticFeedWidget;
      ptabPlasticFeed_->addTab(pwgtPlasticFeedNozzle_[i], "");
    }
    optionTabs_->addTab(ptabPlasticFeed_, "");
    pwgtFiberFeed_ = new OptionsFiberFeedWidget(optionTabs_);
    optionTabs_->addTab(pwgtFiberFeed_, "");
    pwgtFilament_ = new OptionsFilamentWidget(optionTabs_);
    optionTabs_->addTab(pwgtFilament_, "");
    pwgtReinforcingFiber_ = new OptionsReinforcingFiberWidget(optionTabs_);
    optionTabs_->addTab(pwgtReinforcingFiber_, "");

    optionTabs_->setTabShape(QTabWidget::Triangular);
    optionTabs_->setTabPosition(QTabWidget::West);

    plytTop->addWidget(optionTabs_);
  }

  reTranslateUi();
}

void DataWidget::reTranslateUi()
{
  optionTabs_->setTabText(optionTabs_->indexOf(pwgtPrint_), tr("Print"));

  for (size_t i = 0; i < pwgtPlasticFeedNozzle_.size(); ++i)
    ptabPlasticFeed_->setTabText(ptabPlasticFeed_->indexOf(pwgtPlasticFeedNozzle_[i]), tr("Nozzle ") + QString::number(i + 1));
  optionTabs_->setTabText(optionTabs_->indexOf(ptabPlasticFeed_), tr("Plastic\nFeed"));
  optionTabs_->setTabText(optionTabs_->indexOf(pwgtFiberFeed_), tr("Fiber\nFeed"));
  optionTabs_->setTabText(optionTabs_->indexOf(pwgtFilament_), tr("Filament"));
  optionTabs_->setTabText(optionTabs_->indexOf(pwgtReinforcingFiber_), tr("Reinforcing\nFiber"));
}

void DataWidget::readFrom(const Options &options)
{
  pwgtPrint_->readFrom(options.print);
  for (size_t i = 0; i < pwgtPlasticFeedNozzle_.size(); ++i)
    pwgtPlasticFeedNozzle_[i]->readFrom(options.plasticFeed[i]);
  pwgtFiberFeed_->readFrom(options.fiberFeed);
  pwgtFilament_->readFrom(options.filament);
  pwgtReinforcingFiber_->readFrom(options.reinforcingFiber);
}

void DataWidget::writeTo(Options *options) const
{
  pwgtPrint_->writeTo(&options->print);
  for (size_t i = 0; i < pwgtPlasticFeedNozzle_.size(); ++i)
    pwgtPlasticFeedNozzle_[i]->writeTo(&options->plasticFeed[i]);
  pwgtFiberFeed_->writeTo(&options->fiberFeed);
  pwgtFilament_->writeTo(&options->filament);
  pwgtReinforcingFiber_->writeTo(&options->reinforcingFiber);
}
