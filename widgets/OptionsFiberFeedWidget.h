#ifndef OPTIONSFIBERFEEDWIDGET_H
#define OPTIONSFIBERFEEDWIDGET_H

#include <QWidget>

class QLabel;
class QDoubleSpinBox;

struct OptionsFiberFeed;

class OptionsFiberFeedWidget : public QWidget
{
  Q_OBJECT
public:
  explicit OptionsFiberFeedWidget(QWidget *parent = nullptr);
  void readFrom(const OptionsFiberFeed &options);
  void writeTo(OptionsFiberFeed *options) const;

signals:

private:
  QLabel *plblFeedSpeed;
  QDoubleSpinBox *pspnFeedSpeed;
  QLabel *plblShiftZ;
  QDoubleSpinBox *pspnShiftZ;
  QLabel *plblTailLength;
  QDoubleSpinBox *pspnTailLength;

  void reTranslateUi();
};

#endif // OPTIONSFIBERFEEDWIDGET_H
