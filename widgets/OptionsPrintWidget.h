#ifndef OPTIONSPRINTWIDGET_H
#define OPTIONSPRINTWIDGET_H

#include <QWidget>

class QSpinBox;
class QDoubleSpinBox;
class QComboBox;
class QLabel;
class QCheckBox;

class OptionsPrint;

class OptionsPrintWidget : public QWidget
{
  Q_OBJECT
public:
  explicit OptionsPrintWidget(QWidget *parent = nullptr);
  void readFrom(const OptionsPrint &options);
  void writeTo(OptionsPrint *options) const;

signals:

private:
  QLabel *plblLayerThickness;
  QDoubleSpinBox *pspnLayerThickness;
  QLabel *plblFirstLayerThickness;
  QDoubleSpinBox *pspnFirstLayerThickness;
  QLabel *plblOuterPerimetersCount;
  QSpinBox *pspnOuterPerimetersCount;
  QLabel *plblInnerPerimetersCount;
  QSpinBox *pspnInnerPerimetersCount;
  QLabel *plblExtruderThickness;
  QDoubleSpinBox *pspnExtruderThickness;
  QLabel *plblReinforcing;
  QLabel *plblReinforcingLayers;
  QComboBox *pcbxReinforcingLayers;
  QLabel *plblReinforcingVolume;
  QComboBox *pcbxReinforcingVolume;
  QLabel *plblReinforcingType;
  QComboBox *pcbxReinforcingThreadOrientation;
  QLabel *plblReinforcingVolumeType;
  QComboBox *pcbxReinforcingVolumeType;
  QLabel *plblFilling;
  QLabel *plblFillingDensity;
  QSpinBox *pspnFillingDensity;
  QLabel *plblDenseLayersAbove;
  QSpinBox *pspnDenseLayersAbove;
  QLabel *plblDenseLayersBelow;
  QSpinBox *pspnDenseLayersBelow;
  QCheckBox *pchkEnablePlinth;
  QCheckBox *pchkEnableEdge;
  QCheckBox *pchkEnableSupport;

  void reTranslateUi();
};

#endif // OPTIONSPRINTWIDGET_H
