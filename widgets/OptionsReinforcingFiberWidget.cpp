#include "OptionsReinforcingFiberWidget.h"

#include <QLabel>
#include <QGridLayout>
#include <QComboBox>

#include "types/Config.h"

OptionsReinforcingFiberWidget::OptionsReinforcingFiberWidget(QWidget *parent) : QWidget(parent)
{
  QGridLayout *plytTop = new QGridLayout(this);
  {
    plblMaterial1 = new QLabel(this);
    plytTop->addWidget(plblMaterial1, 0, 0, 1, 1);
    pcbxMaterial1 = new QComboBox(this);
    plytTop->addWidget(pcbxMaterial1, 0, 1, 1, 1);

    plytTop->addWidget(new QWidget, 1, 0, 14, 2);
  }

  reTranslateUi();
}

void OptionsReinforcingFiberWidget::readFrom(const OptionsReinforcingFiber &options)
{
  pcbxMaterial1->setCurrentIndex((int)options.material1);
}

void OptionsReinforcingFiberWidget::writeTo(OptionsReinforcingFiber *options) const
{
  options->material1 = (OptionsReinforcingFiber::Material1)pcbxMaterial1->currentIndex();
}

void OptionsReinforcingFiberWidget::reTranslateUi()
{
  plblMaterial1->setText(tr("Parameter 1"));
  pcbxMaterial1->blockSignals(true);
  pcbxMaterial1->clear();
  pcbxMaterial1->addItems(QStringList() << "1" << "2" << "3");
  pcbxMaterial1->blockSignals(false);
}
