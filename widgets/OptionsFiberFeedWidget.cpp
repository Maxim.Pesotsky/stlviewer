#include "OptionsFiberFeedWidget.h"

#include <QGridLayout>
#include <QLabel>
#include <QDoubleSpinBox>

#include "types/Config.h"

OptionsFiberFeedWidget::OptionsFiberFeedWidget(QWidget *parent) : QWidget(parent)
{
  QGridLayout *plytTop = new QGridLayout(this);
  {
    plblFeedSpeed = new QLabel(this);
    plytTop->addWidget(plblFeedSpeed, 0, 0, 1, 1);
    pspnFeedSpeed = new QDoubleSpinBox(this);
    plytTop->addWidget(pspnFeedSpeed, 0, 1, 1, 1);

    plblShiftZ = new QLabel(this);
    plytTop->addWidget(plblShiftZ, 1, 0, 1, 1);
    pspnShiftZ = new QDoubleSpinBox(this);
    plytTop->addWidget(pspnShiftZ, 1, 1, 1, 1);

    plblTailLength = new QLabel(this);
    plytTop->addWidget(plblTailLength, 2, 0, 1, 1);
    pspnTailLength = new QDoubleSpinBox(this);
    plytTop->addWidget(pspnTailLength, 2, 1, 1, 1);

    plytTop->addWidget(new QWidget, 3, 0, 11, 2);
  }

  reTranslateUi();
}

void OptionsFiberFeedWidget::readFrom(const OptionsFiberFeed &options)
{
  pspnFeedSpeed->setValue(options.feedSpeed);
  pspnShiftZ->setValue(options.zShift);
  pspnTailLength->setValue(options.tailLength);
}

void OptionsFiberFeedWidget::writeTo(OptionsFiberFeed *options) const
{
  options->feedSpeed = pspnFeedSpeed->value();
  options->zShift = pspnShiftZ->value();
  options->tailLength = pspnTailLength->value();
}

void OptionsFiberFeedWidget::reTranslateUi()
{
  plblFeedSpeed->setText("Feed speed");
  plblShiftZ->setText("Z-shift (mm)");
  plblTailLength->setText("Tail length");
}
