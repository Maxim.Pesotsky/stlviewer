#ifndef OPTIONSREINFORCINGFIBERWIDGET_H
#define OPTIONSREINFORCINGFIBERWIDGET_H

#include <QWidget>

class QLabel;
class QComboBox;

struct OptionsReinforcingFiber;

class OptionsReinforcingFiberWidget : public QWidget
{
  Q_OBJECT
public:
  explicit OptionsReinforcingFiberWidget(QWidget *parent = nullptr);
  void readFrom(const OptionsReinforcingFiber &options);
  void writeTo(OptionsReinforcingFiber *options) const;

signals:

private:
  QLabel *plblMaterial1;
  QComboBox *pcbxMaterial1;

  void reTranslateUi();
};

#endif // OPTIONSREINFORCINGFIBERWIDGET_H
