#ifndef OPTIONSFILAMENTWIDGET_H
#define OPTIONSFILAMENTWIDGET_H

#include <QWidget>

class QLabel;
class QComboBox;

struct OptionsFilament;

class OptionsFilamentWidget : public QWidget
{
  Q_OBJECT
public:
  explicit OptionsFilamentWidget(QWidget *parent = nullptr);
  void readFrom(const OptionsFilament &options);
  void writeTo(OptionsFilament *options) const;

signals:

private:
  QLabel *plblParameter1;
  QComboBox *pcbxParameter1;
  QLabel *plblParameter2;
  QComboBox *pcbxParameter2;

  void reTranslateUi();
};

#endif // OPTIONSFILAMENTWIDGET_H
