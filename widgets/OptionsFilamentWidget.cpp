#include "OptionsFilamentWidget.h"

#include <QLabel>
#include <QGridLayout>
#include <QComboBox>
#include "types/Config.h"

OptionsFilamentWidget::OptionsFilamentWidget(QWidget *parent) : QWidget(parent)
{
  QGridLayout *plytTop = new QGridLayout(this);
  {
    plblParameter1 = new QLabel(this);
    plytTop->addWidget(plblParameter1, 0, 0, 1, 1);
    pcbxParameter1 = new QComboBox(this);
    plytTop->addWidget(pcbxParameter1, 0, 1, 1, 1);

    plblParameter2 = new QLabel(this);
    plytTop->addWidget(plblParameter2, 1, 0, 1, 1);
    pcbxParameter2 = new QComboBox(this);
    plytTop->addWidget(pcbxParameter2, 1, 1, 1, 1);

    plytTop->addWidget(new QWidget, 2, 0, 13, 2);
  }

  reTranslateUi();
}

void OptionsFilamentWidget::readFrom(const OptionsFilament &options)
{
  pcbxParameter1->setCurrentIndex((int)options.param1);
  pcbxParameter2->setCurrentIndex((int)options.param2);
}

void OptionsFilamentWidget::writeTo(OptionsFilament *options) const
{
  options->param1 = (OptionsFilament::Parametr1)pcbxParameter1->currentIndex();
  options->param2 = (OptionsFilament::Parametr2)pcbxParameter2->currentIndex();
}

void OptionsFilamentWidget::reTranslateUi()
{
  plblParameter1->setText(tr("Parameter 1"));
  pcbxParameter1->blockSignals(true);
  pcbxParameter1->clear();
  pcbxParameter1->addItems(QStringList() << "1" << "2" << "3");
  pcbxParameter1->blockSignals(false);
  plblParameter2->setText(tr("Parameter 2"));
  pcbxParameter2->blockSignals(true);
  pcbxParameter2->clear();
  pcbxParameter2->addItems(QStringList() << "1" << "2" << "3");
  pcbxParameter2->blockSignals(false);
}
