#ifndef OPTIONSPLASTICFEEDWIDGET_H
#define OPTIONSPLASTICFEEDWIDGET_H

#include <QWidget>

class QLabel;
class QCheckBox;
class QDoubleSpinBox;

struct OptionsPlasticFeed;

class OptionsPlasticFeedWidget : public QWidget
{
  Q_OBJECT
public:
  explicit OptionsPlasticFeedWidget(QWidget *parent = nullptr);
  void readFrom(const OptionsPlasticFeed &options);
  void writeTo(OptionsPlasticFeed *options) const;

signals:

private:
  QLabel *plblDiameter;
  QDoubleSpinBox *pspnDiameter;
  QLabel *plblExtruderNozzleTemp;
  QDoubleSpinBox *pspnExtruderNozzleTemp;
  QLabel *plblTempOnLayer;
  QDoubleSpinBox *pspnTempOnLayer;
  QLabel *plblTempOnFirstLayer;
  QDoubleSpinBox *pspnTempOnFirstLayer;
  QCheckBox *pchkEnableCooling;
  QLabel *plblPrintSpeed;
  QLabel *plblPerimeterSpeed;
  QDoubleSpinBox *pspnPerimeterSpeed;
  QLabel *plblFillingSpeed;
  QDoubleSpinBox *pspnFillingSpeed;
  QLabel *plblTransitionSpeed;
  QDoubleSpinBox *pspnTransitionSpeed;
  QLabel *plblShiftZ;
  QDoubleSpinBox *pspnShiftZ;

  void reTranslateUi();
};

#endif // OPTIONSPLASTICFEEDWIDGET_H
