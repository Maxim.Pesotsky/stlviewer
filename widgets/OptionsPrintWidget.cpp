#include "OptionsPrintWidget.h"

#include <QDoubleSpinBox>
#include <QLabel>
#include <QGridLayout>
#include <QComboBox>
#include <QCheckBox>
#include <QHBoxLayout>

#include "types/Config.h"

OptionsPrintWidget::OptionsPrintWidget(QWidget *parent) : QWidget(parent)
{
  QGridLayout *plytTop = new QGridLayout(this);
  {
    plblLayerThickness = new QLabel(this);
    plytTop->addWidget(plblLayerThickness, 0, 0, 1, 1);
    pspnLayerThickness = new QDoubleSpinBox(this);
    plytTop->addWidget(pspnLayerThickness, 0, 1, 1, 1);

    plblFirstLayerThickness = new QLabel(this);
    plytTop->addWidget(plblFirstLayerThickness, 1, 0, 1, 1);
    pspnFirstLayerThickness = new QDoubleSpinBox(this);
    plytTop->addWidget(pspnFirstLayerThickness, 1, 1, 1, 1);

    plblOuterPerimetersCount = new QLabel(this);
    plytTop->addWidget(plblOuterPerimetersCount, 2, 0, 1, 1);
    pspnOuterPerimetersCount = new QSpinBox(this);
    plytTop->addWidget(pspnOuterPerimetersCount, 2, 1, 1, 1);

    plblInnerPerimetersCount = new QLabel(this);
    plytTop->addWidget(plblInnerPerimetersCount, 3, 0, 1, 1);
    pspnInnerPerimetersCount = new QSpinBox(this);
    plytTop->addWidget(pspnInnerPerimetersCount, 3, 1, 1, 1);

    plblExtruderThickness = new QLabel(this);
    plytTop->addWidget(plblExtruderThickness, 4, 0, 1, 1);
    pspnExtruderThickness = new QDoubleSpinBox(this);
    plytTop->addWidget(pspnExtruderThickness, 4, 1, 1, 1);

    plblReinforcing = new QLabel(this);
    plytTop->addWidget(plblReinforcing, 5, 0, 1, 2, Qt::AlignCenter);

    plblReinforcingLayers = new QLabel(this);
    plytTop->addWidget(plblReinforcingLayers, 6, 0, 1, 1);
    pcbxReinforcingLayers = new QComboBox(this);
    plytTop->addWidget(pcbxReinforcingLayers, 6, 1, 1, 1);

    plblReinforcingVolume = new QLabel(this);
    plytTop->addWidget(plblReinforcingVolume, 7, 0, 1, 1);
    pcbxReinforcingVolume = new QComboBox(this);
    plytTop->addWidget(pcbxReinforcingVolume, 7, 1, 1, 1);

    plblReinforcingType = new QLabel(this);
    plytTop->addWidget(plblReinforcingType, 8, 0, 1, 1);
    pcbxReinforcingThreadOrientation = new QComboBox(this);
    plytTop->addWidget(pcbxReinforcingThreadOrientation, 8, 1, 1, 1);

    plblReinforcingVolumeType = new QLabel(this);
    plytTop->addWidget(plblReinforcingVolumeType, 9, 0, 1, 1);
    pcbxReinforcingVolumeType = new QComboBox(this);
    plytTop->addWidget(pcbxReinforcingVolumeType, 9, 1, 1, 1);

    plblFilling = new QLabel(this);
    plytTop->addWidget(plblFilling, 10, 0, 1, 2, Qt::AlignCenter);

    plblFillingDensity = new QLabel(this);
    plytTop->addWidget(plblFillingDensity, 11, 0, 1, 1);
    pspnFillingDensity = new QSpinBox(this);
    plytTop->addWidget(pspnFillingDensity, 11, 1, 1, 1);

    plblDenseLayersAbove = new QLabel(this);
    plytTop->addWidget(plblDenseLayersAbove, 12, 0, 1, 1);
    pspnDenseLayersAbove = new QSpinBox(this);
    plytTop->addWidget(pspnDenseLayersAbove, 12, 1, 1, 1);

    plblDenseLayersBelow = new QLabel(this);
    plytTop->addWidget(plblDenseLayersBelow, 13, 0, 1, 1);
    pspnDenseLayersBelow = new QSpinBox(this);
    plytTop->addWidget(pspnDenseLayersBelow, 13, 1, 1, 1);

    QHBoxLayout *plytExtras = new QHBoxLayout;
    {
      pchkEnablePlinth = new QCheckBox(this);
      plytExtras->addWidget(pchkEnablePlinth, 1);

      pchkEnableEdge = new QCheckBox(this);
      plytExtras->addWidget(pchkEnableEdge, 1);

      pchkEnableSupport = new QCheckBox(this);
      plytExtras->addWidget(pchkEnableSupport, 1);
    }
    plytTop->addLayout(plytExtras, 14, 0, 1, 2);
  }
  reTranslateUi();
}

void OptionsPrintWidget::readFrom(const OptionsPrint &options)
{
  pspnLayerThickness->setValue(options.printLayerThickness);
  pspnFirstLayerThickness->setValue(options.firstLayerThickness);
  pspnInnerPerimetersCount->setValue(options.innerPerimeterCount);
  pspnOuterPerimetersCount->setValue(options.outerPerimeterCount);
  pspnExtruderThickness->setValue(options.extruderThickness);
  pspnFillingDensity->setValue(options.filling.density);
  pspnDenseLayersAbove->setValue(options.filling.denseLayerCountAbove);
  pspnDenseLayersBelow->setValue(options.filling.denseLayerCountBelow);
  pchkEnablePlinth->setChecked(options.filling.plinthEnabled);
  pchkEnableEdge->setChecked(options.filling.edgeEnabled);
  pchkEnableSupport->setChecked(options.filling.supportEnabled);
  pcbxReinforcingLayers->setCurrentIndex((int)options.reinforcing.layers);
  pcbxReinforcingVolume->setCurrentIndex((int)options.reinforcing.volume);
  pcbxReinforcingVolumeType->setCurrentIndex((int)options.reinforcing.volumeType);
  pcbxReinforcingThreadOrientation->setCurrentIndex((int)options.reinforcing.threadOrientation);
}

void OptionsPrintWidget::writeTo(OptionsPrint *options) const
{
  options->printLayerThickness = pspnLayerThickness->value();
  options->firstLayerThickness = pspnFirstLayerThickness->value();
  options->innerPerimeterCount = pspnInnerPerimetersCount->value();
  options->outerPerimeterCount = pspnOuterPerimetersCount->value();
  options->extruderThickness = pspnExtruderThickness->value();
  options->filling.density = pspnFillingDensity->value();
  options->filling.denseLayerCountAbove = pspnDenseLayersAbove->value();
  options->filling.denseLayerCountBelow = pspnDenseLayersBelow->value();
  options->filling.plinthEnabled = pchkEnablePlinth->isChecked();
  options->filling.edgeEnabled = pchkEnableEdge->isChecked();
  options->filling.supportEnabled = pchkEnableSupport->isChecked();
  options->reinforcing.layers = (Reinforcing::Layers)pcbxReinforcingLayers->currentIndex();
  options->reinforcing.volume = (Reinforcing::Volume)pcbxReinforcingVolume->currentIndex();
  options->reinforcing.volumeType = (Reinforcing::VolumeType)pcbxReinforcingVolumeType->currentIndex();
  options->reinforcing.threadOrientation = (Reinforcing::ThreadOrientation)pcbxReinforcingThreadOrientation->currentIndex();
}

void OptionsPrintWidget::reTranslateUi()
{
  plblLayerThickness->setText(tr("Layer thickness (mm)"));
  plblFirstLayerThickness->setText(tr("1st layer thickness (mm)"));
  plblOuterPerimetersCount->setText(tr("Outer perimeters count"));
  plblInnerPerimetersCount->setText(tr("Inner perimeters count"));
  plblExtruderThickness->setText(tr("Extruder thickness (mm)"));
  plblReinforcing->setText(tr("Reinforcing"));
  plblReinforcingLayers->setText(tr("Reinforcing layers"));
  pcbxReinforcingLayers->blockSignals(true);
  pcbxReinforcingLayers->clear();
  pcbxReinforcingLayers->addItems(QStringList() << tr("None") << tr("All") << tr("Selected"));
  pcbxReinforcingLayers->blockSignals(false);
  plblReinforcingVolume->setText(tr("Reinforcing volume"));
  pcbxReinforcingVolume->blockSignals(true);
  pcbxReinforcingVolume->clear();
  pcbxReinforcingVolume->addItems(QStringList() << tr("Inner") << tr("Outer") << tr("Whole"));
  pcbxReinforcingVolume->blockSignals(false);
  plblReinforcingType->setText(tr("Reinforcing type"));
  pcbxReinforcingThreadOrientation->blockSignals(true);
  pcbxReinforcingThreadOrientation->clear();
  pcbxReinforcingThreadOrientation->addItems(QStringList() << "1" << "2" << "3");
  pcbxReinforcingThreadOrientation->blockSignals(false);
  plblReinforcingVolumeType->setText(tr("Reinforcing volume type"));
  pcbxReinforcingVolumeType->blockSignals(true);
  pcbxReinforcingVolumeType->clear();
  pcbxReinforcingVolumeType->addItems(QStringList() << "1" << "2" << "3");
  pcbxReinforcingVolumeType->blockSignals(false);
  plblFilling->setText(tr("Filling"));
  plblFillingDensity->setText(tr("Filling density (%)"));
  plblDenseLayersAbove->setText(tr("Dense layers above"));
  plblDenseLayersBelow->setText(tr("Dense layers below"));
  pchkEnablePlinth->setText(tr("Plinth"));
  pchkEnableEdge->setText(tr("Edge"));
  pchkEnableSupport->setText(tr("Support"));
}
