#include "OptionsPlasticFeedWidget.h"

#include <QGridLayout>
#include <QLabel>
#include <QCheckBox>
#include <QDoubleSpinBox>

#include "types/Config.h"

OptionsPlasticFeedWidget::OptionsPlasticFeedWidget(QWidget *parent) : QWidget(parent)
{
  QGridLayout *plytTop = new QGridLayout(this);
  {
    plblDiameter = new QLabel(this);
    plytTop->addWidget(plblDiameter, 0, 0, 1, 1);
    pspnDiameter = new QDoubleSpinBox(this);
    plytTop->addWidget(pspnDiameter, 0, 1, 1, 1);

    plblExtruderNozzleTemp = new QLabel(this);
    plytTop->addWidget(plblExtruderNozzleTemp, 1, 0, 1, 1);
    pspnExtruderNozzleTemp = new QDoubleSpinBox(this);
    plytTop->addWidget(pspnExtruderNozzleTemp, 1, 1, 1, 1);

    plblTempOnLayer = new QLabel(this);
    plytTop->addWidget(plblTempOnLayer, 2, 0, 1, 1);
    pspnTempOnLayer = new QDoubleSpinBox(this);
    plytTop->addWidget(pspnTempOnLayer, 2, 1, 1, 1);

    plblTempOnFirstLayer = new QLabel(this);
    plytTop->addWidget(plblTempOnFirstLayer, 3, 0, 1, 1);
    pspnTempOnFirstLayer = new QDoubleSpinBox(this);
    plytTop->addWidget(pspnTempOnFirstLayer, 3, 1, 1, 1);

    pchkEnableCooling = new QCheckBox(this);
    plytTop->addWidget(pchkEnableCooling, 4, 0, 1, 2, Qt::AlignCenter);

    plblPrintSpeed = new QLabel(this);
    plytTop->addWidget(plblPrintSpeed, 5, 0, 1, 2);

    plblPerimeterSpeed = new QLabel(this);
    plytTop->addWidget(plblPerimeterSpeed, 6, 0, 1, 1);
    pspnPerimeterSpeed = new QDoubleSpinBox(this);
    plytTop->addWidget(pspnPerimeterSpeed, 6, 1, 1, 1);

    plblFillingSpeed = new QLabel(this);
    plytTop->addWidget(plblFillingSpeed, 7, 0, 1, 1);
    pspnFillingSpeed = new QDoubleSpinBox(this);
    plytTop->addWidget(pspnFillingSpeed, 7, 1, 1, 1);

    plblTransitionSpeed = new QLabel(this);
    plytTop->addWidget(plblTransitionSpeed, 8, 0, 1, 1);
    pspnTransitionSpeed = new QDoubleSpinBox(this);
    plytTop->addWidget(pspnTransitionSpeed, 8, 1, 1, 1);

    plblShiftZ = new QLabel(this);
    plytTop->addWidget(plblShiftZ, 9, 0, 1, 1);
    pspnShiftZ = new QDoubleSpinBox(this);
    plytTop->addWidget(pspnShiftZ, 9, 1, 1, 1);

    plytTop->addWidget(new QWidget, 10, 0, 4, 2);
  }

  reTranslateUi();
}

void OptionsPlasticFeedWidget::readFrom(const OptionsPlasticFeed &options)
{
  pspnDiameter->setValue(options.diameter);
  pspnExtruderNozzleTemp->setValue(options.extruderNozzleTemp);
  pspnTempOnLayer->setValue(options.tempOnPrintLayer);
  pspnTempOnFirstLayer->setValue(options.firstLayerTemp);
  pchkEnableCooling->setChecked(options.isCoolingEnabled);
  pspnPerimeterSpeed->setValue(options.printSpeedOnFilling);
  pspnFillingSpeed->setValue(options.printSpeedOnFilling);
  pspnTransitionSpeed->setValue(options.printSpeedOnTransition);
  pspnShiftZ->setValue(options.zShift);
}

void OptionsPlasticFeedWidget::writeTo(OptionsPlasticFeed *options) const
{
  options->diameter = pspnDiameter->value();
  options->extruderNozzleTemp = pspnExtruderNozzleTemp->value();
  options->tempOnPrintLayer = pspnTempOnLayer->value();
  options->firstLayerTemp = pspnTempOnFirstLayer->value();
  options->isCoolingEnabled = pchkEnableCooling->isChecked();
  options->printSpeedOnPerimeter = pspnPerimeterSpeed->value();
  options->printSpeedOnFilling = pspnFillingSpeed->value();
  options->printSpeedOnTransition = pspnTransitionSpeed->value();
  options->zShift = pspnShiftZ->value();
}

void OptionsPlasticFeedWidget::reTranslateUi()
{
  plblDiameter->setText(tr("Diameter (mm)"));
  plblExtruderNozzleTemp->setText(tr("Extruder nozzle temp. (C)"));
  plblTempOnLayer->setText(tr("Temp. on printed layer (C)"));
  plblTempOnFirstLayer->setText(tr("Temp. on 1st layer (mm)"));
  pchkEnableCooling->setText(tr("Cooling"));
  plblPrintSpeed->setText(tr("Printing speed"));
  plblPerimeterSpeed->setText(tr("-- on perimeter"));
  plblFillingSpeed->setText(tr("-- on filling"));
  plblTransitionSpeed->setText(tr("-- on transition"));
  plblShiftZ->setText(tr("Z-shift (mm)"));
}
