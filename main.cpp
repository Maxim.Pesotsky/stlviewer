
#include <QApplication>
#include <QTranslator>
#include <QDebug>

#include "MainWindow.h"

int main(int argc, char *argv[])
{
  QApplication app(argc, argv);

  QTranslator translator;
  if (translator.load("translation_ru"))
    app.installTranslator(&translator);
  else
    qDebug() << "Ru translation file is not loaded";

  QString modelPath;
  if (argc > 1)
    modelPath = argv[1];

  MainWindow wnd(modelPath, 0);
  wnd.show();
  //app.setAttribute(Qt::AA_UseDesktopOpenGL, true);
  return app.exec();
}
