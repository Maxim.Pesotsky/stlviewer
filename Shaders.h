#ifndef SHADERS_H
#define SHADERS_H

#define xtxt(s) txt(s)
#define txt(s) #s

#define GLSL_REQUIRED_VERSION 330

static const char shaderHeaderSrc[] =
"#version " xtxt(GLSL_REQUIRED_VERSION) "\n"
"#extension GL_ARB_explicit_uniform_location : require\n"
"#extension GL_ARB_explicit_attrib_location : require\n"
"#extension GL_EXT_gpu_shader4 : require\n";

#define ATTR_IN_DRAW_VERTEX_NAME "vertex"
#define ATTR_IN_DRAW_VERTEX_LOCATION 0
#define ATTR_IN_DRAW_NORMAL_NAME "normal"
#define ATTR_IN_DRAW_NORMAL_LOCATION 1
#define ATTR_IN_DRAW_COLOR_NAME "color"
#define ATTR_IN_DRAW_COLOR_LOCATION 2
#define ATTR_IN_DRAW_INDEX_NAME "index"
#define ATTR_IN_DRAW_INDEX_LOCATION 3

#define UNI_DRAW_MVP_NAME "mvpMatrix"
#define UNI_DRAW_MVP_LOCATION 0
#define UNI_DRAW_LIGHTDIR_NAME "lightDir"
#define UNI_DRAW_LIGHTDIR_LOCATION 1

#define ATTR_OUT_DRAW_FRAGMENTCOLOR_NAME "fragColor"
#define ATTR_OUT_DRAW_FRAGMENTCOLOR_LOCATION 0

static const char shaderModelDrawVertexSrc[] =
"layout(location = " xtxt(ATTR_IN_DRAW_VERTEX_LOCATION) ") in vec3 " ATTR_IN_DRAW_VERTEX_NAME ";\n"
"layout(location = " xtxt(ATTR_IN_DRAW_NORMAL_LOCATION) ") in vec3 " ATTR_IN_DRAW_NORMAL_NAME ";\n"
"layout(location = " xtxt(ATTR_IN_DRAW_COLOR_LOCATION) ") in vec4 " ATTR_IN_DRAW_COLOR_NAME ";\n"
///"layout(location = " xtxt(ATTR_IN_DRAW_INDEX_LOCATION) ") in vec4 " ATTR_IN_DRAW_INDEX_NAME ";\n"
"layout(location = " xtxt(UNI_DRAW_MVP_LOCATION) ") uniform mat4 " UNI_DRAW_MVP_NAME ";\n"
"layout(location = " xtxt(UNI_DRAW_LIGHTDIR_LOCATION) ") uniform vec3 " UNI_DRAW_LIGHTDIR_NAME ";\n"
"out vec4 varyingColor;\n"
///"flat out vec4 varyingIndex;\n"
"void main(void)\n"
"{\n"
"  float light = dot(" ATTR_IN_DRAW_NORMAL_NAME ",lightDir);\n"
"  varyingColor = vec4(abs(" ATTR_IN_DRAW_COLOR_NAME ".x*light), -" ATTR_IN_DRAW_COLOR_NAME ".z*light, " ATTR_IN_DRAW_COLOR_NAME ".z*light, " ATTR_IN_DRAW_COLOR_NAME ".a); // lighting object from camera\n"
///"  varyingIndex = index;\n"
"  gl_Position = mvpMatrix*vec4(" ATTR_IN_DRAW_VERTEX_NAME ", 1.0);\n"
"}\0";

static const char shaderModelDrawFragmentSrc[] =
"in vec4 varyingColor;\n"
///"flat in vec4 varyingIndex;\n"
"layout(location = " xtxt(ATTR_OUT_DRAW_FRAGMENTCOLOR_LOCATION) ") out vec4 " ATTR_OUT_DRAW_FRAGMENTCOLOR_NAME ";\n"
"void main(void)\n"
"{\n"
"  " ATTR_OUT_DRAW_FRAGMENTCOLOR_NAME " = varyingColor;\n"
"}\0";

#define ATTR_IN_PICK_VERTEX_NAME "vertex"
#define ATTR_IN_PICK_VERTEX_LOCATION 0
#define ATTR_IN_PICK_INDEX_NAME "index"
#define ATTR_IN_PICK_INDEX_LOCATION 1

#define UNI_PICK_MVP_NAME "mvpMatrix"
#define UNI_PICK_MVP_LOCATION 0

static const char shaderModelPickVertexSrc[] =
"layout(location = " xtxt(ATTR_IN_PICK_VERTEX_LOCATION) ") in vec3 " ATTR_IN_PICK_VERTEX_NAME ";\n"
"layout(location = " xtxt(ATTR_IN_PICK_INDEX_LOCATION) ") in vec4 " ATTR_IN_PICK_INDEX_NAME ";\n"
"layout(location = " xtxt(UNI_PICK_MVP_LOCATION) ") uniform mat4 " UNI_PICK_MVP_NAME ";\n"
"flat out vec4 varyingIndex;\n"
"void main(void)\n"
"{\n"
"  varyingIndex = index;\n"
"  gl_Position = mvpMatrix*vec4(vertex, 1.0);\n"
"}\0";

#define ATTR_OUT_PICK_INDEX_NAME "fragIndex"
#define ATTR_OUT_PICK_INDEX_LOCATION 0

static const char shaderModelPickFragmentSrc[] =
"flat in vec4 varyingIndex;\n"
"layout(location = " xtxt(ATTR_OUT_PICK_INDEX_LOCATION) ") out vec4 " ATTR_OUT_PICK_INDEX_NAME ";\n"
"void main(void)\n"
"{\n"
"  " ATTR_OUT_PICK_INDEX_NAME " = vec4(varyingIndex.x,varyingIndex.y,varyingIndex.z,1.0);\n"
"}\0";

#define ATTR_IN_TABLE_VERTEX_NAME "vertex"
#define ATTR_IN_TABLE_VERTEX_LOCATION 0
#define ATTR_IN_TABLE_TXCOORD_NAME "txcoord"
#define ATTR_IN_TABLE_TXCOORD_LOCATION 1

#define UNI_TABLE_MVP_NAME "mvpMatrix"
#define UNI_TABLE_MVP_LOCATION 0
#define UNI_TABLE_LIGHTDIR_NAME "lightDir"
#define UNI_TABLE_LIGHTDIR_LOCATION 1

#define ATTR_OUT_TABLE_FRAGMENTCOLOR_NAME "fragColor"
#define ATTR_OUT_TABLE_FRAGMENTCOLOR_LOCATION 0

static const char shaderTableVertexSrc[] =
"layout(location = " xtxt(ATTR_IN_TABLE_VERTEX_LOCATION) ") in vec3 " ATTR_IN_TABLE_VERTEX_NAME ";\n"
"layout(location = " xtxt(ATTR_IN_TABLE_TXCOORD_LOCATION) ") in vec2 " ATTR_IN_TABLE_TXCOORD_NAME ";\n"
"layout(location = " xtxt(UNI_TABLE_MVP_LOCATION) ") uniform mat4 " UNI_TABLE_MVP_NAME ";\n"
"layout(location = " xtxt(UNI_TABLE_LIGHTDIR_LOCATION) ") uniform vec3 " UNI_TABLE_LIGHTDIR_NAME ";\n"
"out vec4 varyingLight;\n"
"out vec2 varyingTxCoord;\n"
"void main(void)\n"
"{\n"
"  vec3 normal = vec3(0, 1, 0);\n"
"  vec3 lightDirC = vec3(0, 0, 1);\n"
"  float light = 0.5 + 0.5*dot(normal,lightDir);\n"
"  varyingLight = vec4(light, light, light, 0.5); // lighting object from camera\n"
"  varyingTxCoord = txcoord;\n"
"  gl_Position = mvpMatrix*vec4(" ATTR_IN_TABLE_VERTEX_NAME ", 1.0);\n"
"}\0";

static const char shaderTableFragmentSrc[] =
"in vec4 varyingLight;\n"
"in vec2 varyingTxCoord;\n"
"uniform sampler2D txSampler;\n"
"layout(location = " xtxt(ATTR_OUT_TABLE_FRAGMENTCOLOR_LOCATION) ") out vec4 " ATTR_OUT_TABLE_FRAGMENTCOLOR_NAME ";\n"
"void main(void)\n"
"{\n"
"  " ATTR_OUT_TABLE_FRAGMENTCOLOR_NAME " = texture(txSampler, varyingTxCoord) * varyingLight;\n"
"}\0";

#define ATTR_IN_AXES_VERTEX_NAME "vertex"
#define ATTR_IN_AXES_VERTEX_LOCATION 0
#define ATTR_IN_AXES_NORMAL_NAME "normal"
#define ATTR_IN_AXES_NORMAL_LOCATION 1
#define ATTR_IN_AXES_COLOR_NAME "color"
#define ATTR_IN_AXES_COLOR_LOCATION 2
#define ATTR_IN_AXES_INDEX_NAME "index"
#define ATTR_IN_AXES_INDEX_LOCATION 3

#define UNI_AXES_MVP_NAME "mvpMatrix"
#define UNI_AXES_MVP_LOCATION 0
#define UNI_AXES_LIGHTDIR_NAME "lightDir"
#define UNI_AXES_LIGHTDIR_LOCATION 1

#define ATTR_OUT_AXES_FRAGMENTCOLOR_NAME "fragColor"
#define ATTR_OUT_AXES_FRAGMENTCOLOR_LOCATION 0

static const char shaderAxesVertexSrc[] =
"layout(location = " xtxt(ATTR_IN_AXES_VERTEX_LOCATION) ") in vec3 " ATTR_IN_AXES_VERTEX_NAME ";\n"
"layout(location = " xtxt(ATTR_IN_AXES_NORMAL_LOCATION) ") in vec3 " ATTR_IN_AXES_NORMAL_NAME ";\n"
"layout(location = " xtxt(ATTR_IN_AXES_COLOR_LOCATION) ") in vec4 " ATTR_IN_AXES_COLOR_NAME ";\n"
"layout(location = " xtxt(UNI_AXES_MVP_LOCATION) ") uniform mat4 " UNI_AXES_MVP_NAME ";\n"
"layout(location = " xtxt(UNI_AXES_LIGHTDIR_LOCATION) ") uniform vec3 " UNI_AXES_LIGHTDIR_NAME ";\n"
"out vec4 varyingColor;\n"
"void main(void)\n"
"{\n"
"  float light = dot(" ATTR_IN_AXES_NORMAL_NAME ",lightDir);\n"
"  varyingColor = vec4(" ATTR_IN_AXES_COLOR_NAME ".x*light, " ATTR_IN_AXES_COLOR_NAME ".y*light, " ATTR_IN_AXES_COLOR_NAME ".z*light, " ATTR_IN_AXES_COLOR_NAME ".a); // lighting object from camera\n"
"  vec4 pos = mvpMatrix*vec4(" ATTR_IN_AXES_VERTEX_NAME ", 1.0);\n"
"  gl_Position = pos + vec4(-0.75 * pos.w, -0.75 * pos.w, 0, 0);\n"
"}\0";

static const char shaderAxesFragmentSrc[] =
"in vec4 varyingColor;\n"
"layout(location = " xtxt(ATTR_OUT_AXES_FRAGMENTCOLOR_LOCATION) ") out vec4 " ATTR_OUT_AXES_FRAGMENTCOLOR_NAME ";\n"
"void main(void)\n"
"{\n"
"  " ATTR_OUT_AXES_FRAGMENTCOLOR_NAME " = varyingColor;\n"
"}\0";

#endif // SHADERS_H
