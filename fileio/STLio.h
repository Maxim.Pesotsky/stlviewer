#ifndef STLIO_H
#define STLIO_H

#include <vector>

class QString;
struct Point3f;

bool readSTL(const QString &path, std::vector<Point3f> *vertices);

#endif // STLIO_H
