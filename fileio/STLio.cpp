#include "STLio.h"

#include <chrono>

#include <QDebug>
#include <QFile>
#include <QTextStream>

#include "types/Basic.h"

#pragma pack(push,2)
struct STLPolygon
{
    Point3f n;
    Point3f v1;
    Point3f v2;
    Point3f v3;
    unsigned short atr;
};
#pragma pack(pop)

bool readSTL(const QString &path, std::vector<Point3f> *vertices)
{
  QFile file(path);
  if (!file.open(QIODevice::ReadOnly))
    return false;

  char header[80];
  if (80 != file.read(header, 80))
  {
    qDebug() << "\"stl\" file not a stl file";
    return false;
  }
  uint32_t numberOfTriangles = 0;
  if (sizeof(uint32_t) != file.read((char*)&numberOfTriangles, sizeof(uint32_t)))
  {
    qDebug() << "\"stl\" file not a stl file";
    return false;
  }
  std::vector<STLPolygon> trs;
  const off64_t fileSize = file.size();

  std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();
  if ((80 + sizeof(uint32_t) + sizeof(STLPolygon) * numberOfTriangles) != (unsigned int)fileSize)
  {
    file.seek(0);
    std::vector<char> fileData;
    try { fileData.resize(fileSize); } catch (const std::exception &e) { fileData.clear(); fileData.shrink_to_fit(); }
    if (false && !fileData.empty())
    {
      // on GCC sscanf seems to be faster, than std or Qt streams
      if (fileSize != file.read(fileData.data(), fileSize))
      {
        qDebug() << "\"stl\" file was not read completely";
        return false;
      }
      file.close();
      char buf[32];
      const char *ptr = fileData.data();
      size_t n;
      int ret = sscanf(ptr, "%31s%zn", buf, &n); ptr += n;
      numberOfTriangles = 0;
      while (ret > 0) // EOF <0, and 0 -> means zero read
      {
        ret = sscanf(ptr, "%31s%zn", buf, &n); ptr += n;       // facet || endsolid
        if (memcmp(buf, "facet",5)==0)
        {
          STLPolygon p;
          /*ret = */sscanf(ptr, "%31s%31f%31f%31f%zn", buf,&p.n.x,&p.n.y,&p.n.z, &n); ptr += n;
          /*ret = */sscanf(ptr, "%31s%31s%zn", buf,buf, &n); ptr += n; //hack
          /*ret = */sscanf(ptr, "%31s%31f%31f%31f%zn", buf,&p.v1.x,&p.v1.y,&p.v1.z, &n); ptr += n;
          /*ret = */sscanf(ptr, "%31s%31f%31f%31f%zn", buf,&p.v2.x,&p.v2.y,&p.v2.z, &n); ptr += n;
          /*ret = */sscanf(ptr, "%31s%31f%31f%31f%zn", buf,&p.v3.x,&p.v3.y,&p.v3.z, &n); ptr += n;
          ret =     sscanf(ptr, "%31s%31s%zn", buf,buf, &n); ptr += n; //hack
          trs.push_back(p);
          ++numberOfTriangles;
        }
        else if (memcmp(buf, "endsolid",8)==0)
          break;
      }
    }
    else
    {
      // looks like TextStream also loads file completely and much faster than fstream or ffscanf, but a bit slower than sscanf from preloaded file
      const QByteArray solid("solid"), facet("facet"), endsolid("endsolid");
      QByteArray buf;
      QTextStream stream(&file);
      stream >> buf; // solid
      if (solid != buf)
      {
        qDebug() << "\"stl\" file not a stl file";
        return false;
      }
      numberOfTriangles = 0;
      while (!stream.atEnd()) // EOF <0, and 0 -> means zero read
      {
        stream >> buf; // facet || endsolid
        if (facet == buf)
        {
          trs.emplace_back();
          STLPolygon &p = trs.back();
          stream >> buf >> p.n.x >> p.n.y >> p.n.z; // "normal" x y z
          stream >> buf >> buf; // "outer" "loop"
          stream >> buf >> p.v1.x >> p.v1.y >> p.v1.z; // "vertex" x y z
          stream >> buf >> p.v2.x >> p.v2.y >> p.v2.z; // "vertex" x y z
          stream >> buf >> p.v3.x >> p.v3.y >> p.v3.z; // "vertex" x y z
          stream >> buf >> buf; // "endloop" "endfacet"
          ++numberOfTriangles;
        }
        else if (endsolid == buf)
          break;
      }
    }
  }
  else
  {
    // binary stl
    trs.resize(numberOfTriangles);
    file.seek(80 + sizeof(uint32_t));
    if ((sizeof(STLPolygon) * numberOfTriangles) != file.read((char*)trs.data(), sizeof(STLPolygon) * numberOfTriangles))
    {
      qDebug() << "binary stl file read error";
      return false;
    }
  }
  std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();
  qDebug() << "STL load time:" << 0.001 * (double)std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count() << "s; polygon count:" << numberOfTriangles;

  if (file.isOpen())
    file.close();

  vertices->resize(numberOfTriangles * 3);
  for (unsigned int i = 0; i < numberOfTriangles; ++i)
  {
    const STLPolygon &t = trs[i];

    (*vertices)[i * 3] = t.v1;
    (*vertices)[i * 3 + 1] = t.v2;
    (*vertices)[i * 3 + 2] = t.v3;
  }

  return true;
}
