#ifndef BASIC_H
#define BASIC_H

#include <GL/gl.h>

struct Point2f { float x, y; };
struct Point3f { float x, y, z; };
struct Angle3f { float a, b, g; };
struct Color4b { GLubyte r, g, b, a; };

using Vector3f = Point3f;

static Point3f operator+(const Point3f &a, const Point3f &b) { return Point3f{a.x + b.x, a.y + b.y, a.z + b.z}; }
static Point3f operator/(const Point3f &a, const float b) { return Point3f{a.x / b, a.y / b, a.z / b}; }

static bool operator!=(const Color4b &a, const Color4b &b) { return (a.r != b.r) | (a.g != b.g) | (a.b != b.b) | (a.a != b.a); }
static bool operator==(const Color4b &a, const Color4b &b) { return (a.r == b.r) & (a.g == b.g) & (a.b == b.b) & (a.a == b.a); }


#endif // BASIC_H
