#include "Config.h"

#include <QXmlStreamReader>
#include <QXmlStreamWriter>

#include <QDebug>

template<typename T> T get(const QStringRef &str, bool *ok);
template<> double get<double>(const QStringRef &str, bool *ok) { return str.toDouble(ok); }
template<> uint32_t get<uint32_t>(const QStringRef &str, bool *ok) { return str.toUInt(ok); }
template<> uint8_t get<uint8_t>(const QStringRef &str, bool *ok) { return (uint8_t)str.toUShort(ok); }
template<> bool get<bool>(const QStringRef &str, bool *ok) { return (bool)str.toUShort(ok); }

template<typename T>
static bool readXml(QXmlStreamReader *stream, T *value)
{
  bool ok = true;
  while (!stream->atEnd() && !(ok = stream->isStartElement()))
    stream->readNext();
  if (!ok)
    return false;
  const QStringRef name = stream->name();
  while (!stream->atEnd() && !(ok = (stream->isCharacters() && !stream->isWhitespace())))
    stream->readNext();
  if (!ok)
    return false;
  if (value)
  {
    QStringRef buf = stream->text();
    *value = get<T>(buf, &ok);
  }
  if (!ok)
    return false;
  while (!stream->atEnd() && !(ok = stream->isEndElement()))
    stream->readNext();
  return ok && (stream->name() == name);
}

template<>
bool readXml<Reinforcing>(QXmlStreamReader *stream, Reinforcing *value)
{
  if (stream->name() != "Reinforcing")
    return false;

  while (!stream->atEnd())
  {
    stream->readNext();
    if (stream->isStartElement())
    {
      if ("Layers" == stream->name())
      {
        if (!readXml<uint8_t>(stream, (uint8_t*)&value->layers))
          return false;
      }
      else if ("Volume" == stream->name())
      {
        if (!readXml<uint8_t>(stream, (uint8_t*)&value->volume))
          return false;
      }
      else if ("ThreadOrientation" == stream->name())
      {
        if (!readXml<uint8_t>(stream, (uint8_t*)&value->threadOrientation))
          return false;
      }
      else if ("VolumeType" == stream->name())
      {
        if (!readXml<uint8_t>(stream, (uint8_t*)&value->volumeType))
          return false;
      }
      else
      {
        qDebug() << "Unknown tag " << stream->name();
        return false;
      }
    }
    else if (stream->isEndElement())
      break;
  }
  return stream->name() == "Reinforcing";
}

template<>
bool readXml<Filling>(QXmlStreamReader *stream, Filling *value)
{
  if (stream->name() != "Filling")
    return false;

  while (!stream->atEnd())
  {
    stream->readNext();
    if (stream->isStartElement())
    {
      if ("Density" == stream->name())
      {
        if (!readXml<double>(stream, &value->density))
          return false;
      }
      else if ("DenseLayerCountAbove" == stream->name())
      {
        if (!readXml<uint32_t>(stream, &value->denseLayerCountAbove))
          return false;
      }
      else if ("DenseLayerCountBelow" == stream->name())
      {
        if (!readXml<uint32_t>(stream, &value->denseLayerCountBelow))
          return false;
      }
      else if ("PlinthEnabled" == stream->name())
      {
        if (!readXml<bool>(stream, &value->plinthEnabled))
          return false;
      }
      else if ("EdgeEnabled" == stream->name())
      {
        if (!readXml<bool>(stream, &value->edgeEnabled))
          return false;
      }
      else if ("SupportEnabled" == stream->name())
      {
        if (!readXml<bool>(stream, &value->supportEnabled))
          return false;
      }
    }
    else if (stream->isEndElement())
      break;
  }
  return stream->name() == "Filling";
}

template<>
bool readXml<OptionsReinforcingFiber>(QXmlStreamReader *stream, OptionsReinforcingFiber *value)
{
  if (stream->name() != "ReinforcingFiber")
    return false;

  while (!stream->atEnd())
  {
    stream->readNext();
    if (stream->isStartElement())
    {
      if ("Material1" == stream->name())
      {
        if (!readXml<uint8_t>(stream, (uint8_t*)&value->material1))
          return false;
      }
      else
      {
        qDebug() << "Unknown tag " << stream->name();
        return false;
      }
    }
    else if (stream->isEndElement())
      break;
  }
  return stream->name() == "ReinforcingFiber";
}

template<>
bool readXml<OptionsFilament>(QXmlStreamReader *stream, OptionsFilament *value)
{
  if (stream->name() != "Filament")
    return false;

  while (!stream->atEnd())
  {
    stream->readNext();
    if (stream->isStartElement())
    {
      if ("Param1" == stream->name())
      {
        if (!readXml<uint8_t>(stream, (uint8_t*)&value->param1))
          return false;
      }
      else if ("Param2" == stream->name())
      {
        if (!readXml<uint8_t>(stream, (uint8_t*)&value->param2))
          return false;
      }
      else
      {
        qDebug() << "Unknown tag " << stream->name();
        return false;
      }
    }
    else if (stream->isEndElement())
      break;
  }
  return stream->name() == "Filament";
}

template<>
bool readXml<OptionsFiberFeed>(QXmlStreamReader *stream, OptionsFiberFeed *value)
{
  if (stream->name() != "FiberFeed")
    return false;

  while (!stream->atEnd())
  {
    stream->readNext();
    if (stream->isStartElement())
    {
      if ("FeedSpeed" == stream->name())
      {
        if (!readXml<double>(stream, &value->feedSpeed))
          return false;
      }
      else if ("ZShift" == stream->name())
      {
        if (!readXml<double>(stream, &value->zShift))
          return false;
      }
      else if ("TailLength" == stream->name())
      {
        if (!readXml<double>(stream, &value->tailLength))
          return false;
      }
      else
      {
        qDebug() << "Unknown tag " << stream->name();
        return false;
      }
    }
    else if (stream->isEndElement())
      break;
  }
  return stream->name() == "FiberFeed";
}

template<>
bool readXml<OptionsPlasticFeed>(QXmlStreamReader *stream, OptionsPlasticFeed *value)
{
  if (stream->name() != "PlasticFeed")
    return false;

  while (!stream->atEnd())
  {
    stream->readNext();
    if (stream->isStartElement())
    {
      if ("Diameter" == stream->name())
      {
        if (!readXml<double>(stream, &value->diameter))
          return false;
      }
      else if ("ExtruderNozzleTemperature" == stream->name())
      {
        if (!readXml<double>(stream, &value->extruderNozzleTemp))
          return false;
      }
      else if ("TemperatureOnPrintLayer" == stream->name())
      {
        if (!readXml<double>(stream, &value->tempOnPrintLayer))
          return false;
      }
      else if ("FirstLayerTemperature" == stream->name())
      {
        if (!readXml<double>(stream, &value->firstLayerTemp))
          return false;
      }
      else if ("PrintSpeedOnPerimeter" == stream->name())
      {
        if (!readXml<double>(stream, &value->printSpeedOnPerimeter))
          return false;
      }
      else if ("PrintSpeedOnFilling" == stream->name())
      {
        if (!readXml<double>(stream, &value->printSpeedOnFilling))
          return false;
      }
      else if ("PrintSpeedOnTransition" == stream->name())
      {
        if (!readXml<double>(stream, &value->printSpeedOnTransition))
          return false;
      }
      else if ("ZShift" == stream->name())
      {
        if (!readXml<double>(stream, &value->zShift))
          return false;
      }
      else if ("CoolingEnabled" == stream->name())
      {
        if (!readXml<bool>(stream, &value->isCoolingEnabled))
          return false;
      }
      else
      {
        qDebug() << "Unknown tag " << stream->name();
        return false;
      }
    }
    else if (stream->isEndElement())
      break;
  }
  return stream->name() == "PlasticFeed";
}

template<>
bool readXml<OptionsPrint>(QXmlStreamReader *stream, OptionsPrint *value)
{
  if (stream->name() != "Print")
    return false;

  while (!stream->atEnd())
  {
    stream->readNext();
    if (stream->isStartElement())
    {
      if ("LayerThickness" == stream->name())
      {
        if (!readXml<double>(stream, &value->printLayerThickness))
          return false;
      }
      else if ("FirstLayerThickness" == stream->name())
      {
        if (!readXml<double>(stream, &value->firstLayerThickness))
          return false;
      }
      else if ("OuterPerimeterCount" == stream->name())
      {
        if (!readXml<uint32_t>(stream, &value->outerPerimeterCount))
          return false;
      }
      else if ("InnerPerimeterCount" == stream->name())
      {
        if (!readXml<uint32_t>(stream, &value->innerPerimeterCount))
          return false;
      }
      else if ("ExtruderThickness" == stream->name())
      {
        if (!readXml<double>(stream, &value->extruderThickness))
          return false;
      }
      else if ("Reinforcing" == stream->name())
      {
        if (!readXml<Reinforcing>(stream, &value->reinforcing))
          return false;
      }
      else if ("Filling" == stream->name())
      {
        if (!readXml<Filling>(stream, &value->filling))
          return false;
      }
      else
      {
        qDebug() << "Unknown tag " << stream->name();
        return false;
      }
    }
    else if (stream->isEndElement())
      break;
  }
  return stream->name() == "Print";
}

static bool readXml(QXmlStreamReader *stream, Options *value)
{
  if (stream->name() != "Config")
    return false;

  while (!stream->atEnd())
  {
    stream->readNext();

    //qDebug() << stream->name() << "start" << stream->isStartElement() << "end" << stream->isEndElement() << "characters" << stream->isCharacters();
    //continue;

    if (stream->isStartElement())
    {
      if ("Print" == stream->name())
      {
        if (!readXml<OptionsPrint>(stream, &value->print))
          return false;
      }
      else if ("PlasticFeed" == stream->name())
      {
        bool ok;
        uint16_t nozzle = stream->attributes().value("Nozzle").toUShort(&ok);
        if ((nozzle < 1) | (nozzle > value->plasticFeed.size()))
          return false;
        if (!readXml<OptionsPlasticFeed>(stream, &value->plasticFeed[nozzle - 1]))
          return false;
      }
      else if ("FiberFeed" == stream->name())
      {
        if (!readXml<OptionsFiberFeed>(stream, &value->fiberFeed))
          return false;
      }
      else if ("Filament" == stream->name())
      {
        if (!readXml<OptionsFilament>(stream, &value->filament))
          return false;
      }
      else if ("ReinforcingFiber" == stream->name())
      {
        if (!readXml<OptionsReinforcingFiber>(stream, &value->reinforcingFiber))
          return false;
      }
      else
      {
        qDebug() << "Unknown tag " << stream->name();
        return false;
      }
    }
    else if (stream->isEndElement())
      break;
  }
  return stream->name() == "Config";
}

bool readXml(const QString &src, Options *dst)
{
  QXmlStreamReader stream(src);
  if (!stream.readNextStartElement())
    return false;
  return readXml(&stream, dst);
}

bool writeXml(const Options &src, QString *dst)
{
  if (!dst)
    return false;

  QXmlStreamWriter stream(dst);
  stream.setAutoFormatting(true);
  stream.setAutoFormattingIndent(1);
  stream.writeStartDocument();

  stream.writeStartElement("Config");

  // Print
  stream.writeStartElement("Print");
  stream.writeTextElement("LayerThickness", QString::number(src.print.printLayerThickness));
  stream.writeTextElement("FirstLayerThickness", QString::number(src.print.firstLayerThickness));
  stream.writeTextElement("OuterPerimeterCount", QString::number(src.print.outerPerimeterCount));
  stream.writeTextElement("InnerPerimeterCount", QString::number(src.print.innerPerimeterCount));
  stream.writeTextElement("ExtruderThickness", QString::number(src.print.extruderThickness));

  stream.writeStartElement("Reinforcing");
  stream.writeTextElement("Layers", QString::number((uint8_t)src.print.reinforcing.layers));
  stream.writeTextElement("Volume", QString::number((uint8_t)src.print.reinforcing.volume));
  stream.writeTextElement("ThreadOrientation", QString::number((uint8_t)src.print.reinforcing.threadOrientation));
  stream.writeTextElement("VolumeType", QString::number((uint8_t)src.print.reinforcing.volumeType));
  stream.writeEndElement(); // Reinforcing

  stream.writeStartElement("Filling");
  stream.writeTextElement("Density", QString::number(src.print.filling.density));
  stream.writeTextElement("DenseLayerCountAbove", QString::number(src.print.filling.denseLayerCountAbove));
  stream.writeTextElement("DenseLayerCountBelow", QString::number(src.print.filling.denseLayerCountBelow));
  stream.writeTextElement("PlinthEnabled", QString::number((uint8_t)src.print.filling.plinthEnabled));
  stream.writeTextElement("EdgeEnabled", QString::number((uint8_t)src.print.filling.edgeEnabled));
  stream.writeTextElement("SupportEnabled", QString::number((uint8_t)src.print.filling.supportEnabled));
  stream.writeEndElement(); // Filling
  stream.writeEndElement(); // Print

  // Plastic Feed
  for (size_t i = 0; i < src.plasticFeed.size(); ++i)
  {
    stream.writeStartElement("PlasticFeed");
    stream.writeAttribute("Nozzle", QString::number(i + 1));
    stream.writeTextElement("Diameter", QString::number(src.plasticFeed[i].diameter));
    stream.writeTextElement("ExtruderNozzleTemperature", QString::number(src.plasticFeed[i].extruderNozzleTemp));
    stream.writeTextElement("TemperatureOnPrintLayer", QString::number(src.plasticFeed[i].tempOnPrintLayer));
    stream.writeTextElement("FirstLayerTemperature", QString::number(src.plasticFeed[i].firstLayerTemp));
    stream.writeTextElement("PrintSpeedOnPerimeter", QString::number(src.plasticFeed[i].printSpeedOnPerimeter));
    stream.writeTextElement("PrintSpeedOnFilling", QString::number(src.plasticFeed[i].printSpeedOnFilling));
    stream.writeTextElement("PrintSpeedOnTransition", QString::number(src.plasticFeed[i].printSpeedOnTransition));
    stream.writeTextElement("ZShift", QString::number(src.plasticFeed[i].zShift));
    stream.writeTextElement("CoolingEnabled", QString::number(src.plasticFeed[i].isCoolingEnabled));
    stream.writeEndElement(); // Plastic Feed
  }

  // Fiber Feed
  stream.writeStartElement("FiberFeed");
  stream.writeTextElement("FeedSpeed", QString::number(src.fiberFeed.feedSpeed));
  stream.writeTextElement("ZShift", QString::number(src.fiberFeed.zShift));
  stream.writeTextElement("TailLength", QString::number(src.fiberFeed.tailLength));
  stream.writeEndElement(); // Fiber Feed

  // Filament
  stream.writeStartElement("Filament");
  stream.writeTextElement("Param1", QString::number((uint8_t)src.filament.param1));
  stream.writeTextElement("Param2", QString::number((uint8_t)src.filament.param2));
  stream.writeEndElement(); // Filament

  // Reinforcing Fiber
  stream.writeStartElement("ReinforcingFiber");
  stream.writeTextElement("Material1", QString::number((uint8_t)src.reinforcingFiber.material1));
  stream.writeEndElement(); // Reinforcing Fiber

  stream.writeEndElement(); // Config

  stream.writeEndDocument();
  return true;
}
