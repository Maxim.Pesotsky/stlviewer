#ifndef CONFIG_H
#define CONFIG_H

#include <cstdint>
#include <array>

class QString;

struct Reinforcing
{
  enum class Layers : uint8_t { None, All, Selected, } layers;
  enum class Volume : uint8_t { Inner, Outer, Whole, } volume;
  enum class ThreadOrientation : uint8_t { _1, _2, _3, } threadOrientation;
  enum class VolumeType : uint8_t { _1, _2, _3, } volumeType;
  Reinforcing() :
    layers(Layers::None),
    volume(Volume::Inner),
    threadOrientation(ThreadOrientation::_1),
    volumeType(VolumeType::_1)
  {}
};

struct Filling
{
  double density;
  uint32_t denseLayerCountAbove;
  uint32_t denseLayerCountBelow;
  bool plinthEnabled;
  bool edgeEnabled;
  bool supportEnabled;
  Filling() :
    density(0),
    denseLayerCountAbove(0),
    denseLayerCountBelow(0),
    plinthEnabled(false),
    edgeEnabled(false),
    supportEnabled(false)
  {}
};

struct OptionsPrint
{
  double printLayerThickness;
  double firstLayerThickness;
  uint32_t outerPerimeterCount;
  uint32_t innerPerimeterCount;
  double extruderThickness;
  Reinforcing reinforcing;
  Filling filling;
  OptionsPrint() :
    printLayerThickness(0),
    firstLayerThickness(0),
    outerPerimeterCount(0),
    innerPerimeterCount(0),
    extruderThickness(0),
    reinforcing(),
    filling()
  {}
};

struct OptionsPlasticFeed
{
  double diameter;
  double extruderNozzleTemp;
  double tempOnPrintLayer;
  double firstLayerTemp;
  double printSpeedOnPerimeter;
  double printSpeedOnFilling;
  double printSpeedOnTransition;
  double zShift;
  bool isCoolingEnabled;
  OptionsPlasticFeed() :
    diameter(0),
    extruderNozzleTemp(0),
    tempOnPrintLayer(0),
    firstLayerTemp(0),
    printSpeedOnPerimeter(0),
    printSpeedOnFilling(0),
    printSpeedOnTransition(0),
    zShift(0),
    isCoolingEnabled(false)
  {}
};

struct OptionsFiberFeed
{
  double feedSpeed;
  double zShift;
  double tailLength;
  OptionsFiberFeed() :
    feedSpeed(0),
    zShift(0),
    tailLength(0)
  {}
};

struct OptionsFilament
{
  enum class Parametr1 : uint8_t { _1, _2, _3, } param1;
  enum class Parametr2 : uint8_t { _1, _2, _3, } param2;
  OptionsFilament() :
    param1(Parametr1::_1),
    param2(Parametr2::_1)
  {}
};

struct OptionsReinforcingFiber
{
  enum class Material1 : uint8_t { _1, _2, _3, } material1;
  OptionsReinforcingFiber() :
    material1(Material1::_1)
  {}
};

struct Options
{
  OptionsPrint print;
  std::array<OptionsPlasticFeed, 2> plasticFeed;
  OptionsFiberFeed fiberFeed;
  OptionsFilament filament;
  OptionsReinforcingFiber reinforcingFiber;
  Options() :
    print(),
    plasticFeed(),
    fiberFeed(),
    filament(),
    reinforcingFiber()
  {}
};

bool readXml(const QString &src, Options *dst);
bool writeXml(const Options &src, QString *dst);

#endif // CONFIG_H
