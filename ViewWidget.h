#ifndef VIEWWIDGET_H
#define VIEWWIDGET_H

#include <chrono>

#include <QOpenGLWidget>
#include <QOpenGLShaderProgram>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLBuffer>
#include <QOpenGLTexture>

#include "types/Basic.h"

class QWidget;
class QMenu;
class QOpenGLFramebufferObject;
class QOpenGLFunctions_3_2_Core;

constexpr Color4b SelectedColor{255, 255, 0, 255};
constexpr Color4b DefaultColor{0, 0, 255, 255};

class ViewWidget : public QOpenGLWidget
{
  Q_OBJECT
  using BaseClass = QOpenGLWidget;
  enum class MOUSE_MODE : uint8_t { ROTATE, SELECT };

public:
  ViewWidget(const QString &modelPath = QLatin1Literal(""), QWidget *parent = nullptr);
  ~ViewWidget();

  bool loadSTLFile(const QString &path);
  void setTableGridStep(int step);
  void setTableVisible(bool visible);

protected:
  void initializeGL() override;
  void resizeGL(int w, int h) override;
  void paintGL() override;
  virtual void mousePressEvent(QMouseEvent * event) override;
  virtual void mouseReleaseEvent(QMouseEvent * event) override;
  virtual void mouseMoveEvent(QMouseEvent * event) override;
  virtual void wheelEvent(QWheelEvent * event) override;

private:
  struct ModelData {
    QString path;
    std::vector<Color4b> colors;
    std::vector<Point3f> vertices;
  } modelData_;

  struct ModelGL {
    QOpenGLShaderProgram programDraw, programPick;
    QOpenGLVertexArrayObject vaoDraw, vaoPick;
    QOpenGLBuffer vertices, colors, normals, indices;
  } modelGL_;

  struct TableData {
    uint32_t gridStep;
    uint8_t lineWidth;
  } tableData_;

  struct TableGL
  {
    QOpenGLShaderProgram program;
    QOpenGLVertexArrayObject vao;
    QOpenGLBuffer vertices, txcoords;
    std::unique_ptr<QOpenGLTexture> texture;
    bool visible;
  } tableGL_;

  struct AxesData
  {
    GLsizei vertexCount;
    QMatrix4x4 ptMatrix; // fixed projection + translation
    bool detached; // if true, use fixed projection + translation instead of commons
  } axesData_;

  struct AxisArrowGL
  {
    QOpenGLShaderProgram program;
    QOpenGLVertexArrayObject vao;
    QOpenGLBuffer vertices, colors, normals;
    bool visible;
  } axesGL_;

  std::unique_ptr<QOpenGLFramebufferObject> indexFrameBuffer_;
  std::unique_ptr<QOpenGLFunctions_3_2_Core> gleFunctions;

  bool pickBufferValid_;

  Point3f cameraPosition_;
  Angle3f modelOrientation_;
  float radius_;
  float brushSize_;

  QMatrix4x4 pMatrix_;
  QMatrix4x4 tMatrix_;
  QMatrix4x4 rMatrix_;
  QVector3D lightDir_;
  QVector3D OX_, OY_, OZ_;

  MOUSE_MODE mouseMode_;
  QPoint lastMousePos_;
  Qt::MouseButtons pressedButtons_;
  std::chrono::steady_clock::time_point lastMouseTime_;
  int lastSelectedPolygon_;

  QMenu *contextMenu_;

  bool renderIndices();
  int getPolygonIndex(const int x, const int y);
  void updateRotationAngles();
  void reRotate();
  void rePerspective();
  void reTranslate();
  Point3f restorePerspectivePoint(const int x, const int y, const float depth) const;
  void setPolygonColor(const size_t ind, const Color4b &color);
  bool syncModel();
  bool createTable();
  bool createAxesArrows();
  QMenu* getContextMenu();

  void layOnPolygon();
  void turnUpsideDown();
  void resetMVP();
  void dumpDepth(const QString &path);
};

#endif // VIEWWIDGET_H
