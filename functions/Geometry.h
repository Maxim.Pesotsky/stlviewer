#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <cmath>

#include <QMatrix4x4>

#include "types/Basic.h"

static Angle3f extractRotationAngles(const QMatrix4x4 &mat, bool toDegrees)
{
  Angle3f angles;
  const float coef = (float)(toDegrees ? (180.0 / M_PI) : 1);
  const float m00 = mat(0, 0);
  const float m01 = mat(0, 1);
  const float m02 = mat(0, 2);
  const float m12 = mat(1, 2);
  const float m22 = mat(2, 2);

  angles.g = std::atan2(-m01, m00) * coef;
  angles.a = std::atan2(-m12, m22) * coef;
  angles.b = std::asin(m02) * coef;
  return angles;
}

static Vector3f cross(const Vector3f &a, const Vector3f &b)
{
  return Vector3f {
    a.y * b.z - a.z * b.y,
    a.z * b.x - a.x * b.z,
    a.x * b.y - a.y * b.x,
  };
}

static float dot(const Vector3f &a, const Vector3f &b)
{
  return a.x * b.x + a.y * b.y + a.z * b.z;
}

static Point3f normal(const Point3f &v0, const Point3f &v1, const Point3f &v2)
{
  Point3f n;
  n.x = (v1.y - v0.y) *( v2.z - v0.z) - (v1.z - v0.z) * (v2.y - v0.y);
  n.y = (v1.z - v0.z) * (v2.x - v0.x) - (v1.x - v0.x) * (v2.z - v0.z);
  n.z = (v1.x - v0.x) * (v2.y - v0.y) - (v1.y - v0.y) * (v2.x - v0.x);
  const float il = 1.0f / std::sqrt(n.x * n.x + n.y * n.y + n.z * n.z);
  n.x *= il; n.y *= il; n.z *= il;
  return n;
}

static float distance(const Point3f &v1, const Point3f &v2)
{
  const float dx = v1.x - v2.x;
  const float dy = v1.y - v2.y;
  const float dz = v1.z - v2.z;
  return std::sqrt(dx * dx + dy * dy + dz * dz);
}

static float length(const Vector3f &v)
{
  return std::sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
}

static void normalize(Vector3f *v)
{
  const float l = length(*v);
  v->x /= l;
  v->y /= l;
  v->z /= l;
}

static Angle3f toEuler(double x,double y,double z,double angle)
{
  double heading, attitude, bank;
  double s= std::sin(angle);
  double c= std::cos(angle);
  double t=1-c;
  //  if axis is not already normalised then uncomment this
  // double magnitude = Math.sqrt(x*x + y*y + z*z);
  // if (magnitude==0) throw error;
  // x /= magnitude;
  // y /= magnitude;
  // z /= magnitude;
  if ((x*y*t + z*s) > 0.998) { // north pole singularity detected
    heading = 2*atan2(x*std::sin(angle/2), std::cos(angle/2));
    attitude = M_PI/2;
    bank = 0;
    return Angle3f{(float)heading, (float)attitude, (float)bank};
  }
  if ((x*y*t + z*s) < -0.998) { // south pole singularity detected
    heading = -2*atan2(x*std::sin(angle/2),std::cos(angle/2));
    attitude = -M_PI/2;
    bank = 0;
    return Angle3f{(float)heading, (float)attitude, (float)bank};
  }
  heading = std::atan2(y * s- x * z * t , 1 - (y*y+ z*z ) * t);
  attitude = std::asin(x * y * t + z * s) ;
  bank = std::atan2(x * s - y * z * t , 1 - (x*x + z*z) * t);
  return Angle3f{(float)bank, (float)heading, (float)attitude};
}

#endif // GEOMETRY_H
