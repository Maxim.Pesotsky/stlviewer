QT += widgets opengl xml

CONFIG += c++14

#QMAKE_CXXFLAGS += -fno-omit-frame-pointer -fsanitize=address -g
#QMAKE_LFLAGS += -fsanitize=address

QMAKE_CXXFLAGS += -Wno-unused-function

HEADERS += \
  AppConfiguration.h \
  DataWidget.h \
  MainWindow.h \
  Shaders.h \
  ViewWidget.h \
  fileio/STLio.h \
  functions/Geometry.h \
  types/Basic.h \
  types/Config.h \
  widgets/OptionsFiberFeedWidget.h \
  widgets/OptionsFilamentWidget.h \
  widgets/OptionsPlasticFeedWidget.h \
  widgets/OptionsPrintWidget.h \
  widgets/OptionsReinforcingFiberWidget.h

SOURCES += \
  DataWidget.cpp \
  MainWindow.cpp \
  ViewWidget.cpp \
  fileio/STLio.cpp \
  main.cpp \
  types/Config.cpp \
  widgets/OptionsFiberFeedWidget.cpp \
  widgets/OptionsFilamentWidget.cpp \
  widgets/OptionsPlasticFeedWidget.cpp \
  widgets/OptionsPrintWidget.cpp \
  widgets/OptionsReinforcingFiberWidget.cpp

TRANSLATIONS = translation_ru.ts

RESOURCES += \
  resources.qrc

