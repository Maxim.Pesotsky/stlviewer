#include "ViewWidget.h"

#include <cmath>
#include <chrono>

#include <QMenu>
#include <QFile>
#include <QDebug>
#include <QString>
#include <QMouseEvent>
#include <QOpenGLFunctions_3_2_Core>
#include <QOpenGLFramebufferObject>
#include <QOpenGLTexture>

#include "Shaders.h"
#include "AppConfiguration.h"
#include "functions/Geometry.h"
#include "fileio/STLio.h"

#define CHECK_GL_ERROR(TXT)\
    { GLenum errcode = glGetError();\
    if (errcode != GL_NO_ERROR)\
      qDebug() << "OpenGL error: " << QString::number(errcode) << ": " << (TXT);}

static int glslVersionStringToNumber(const char *s)
{
  int majmin[2] = {0, 0};
  for (int i = 0; i < 2; ++i)
  {
    while ((*s != 0) & ( (*s < '0') | (*s > '9')))
      ++s;
    if (*s==0)
      return -1;
    while ((*s >= '0')&(*s <= '9'))
    {
      majmin[i] *= 10;
      majmin[i] += *s - '0';
      ++s;
    }
  }
  return majmin[0] * 100 + majmin[1];
}

static uint32_t colorToIndex(const Color4b &color)
{
  return color.r * 65536 + color.g * 256 + color.b;
}

static Color4b indexToColor(uint32_t idx)
{
  Color4b c;
  c.r = idx / 65536 % 256;
  c.g = idx / 256 % 256;
  c.b = idx % 256;
  c.a = 1;
  return c;
}

ViewWidget::ViewWidget(const QString &modelPath, QWidget *parent) : QOpenGLWidget(parent),
  lightDir_{0, 0, 1},
  OX_{1, 0, 0},
  OY_{0, 1, 0},
  OZ_{0, 0, 1}
{
  mouseMode_ = MOUSE_MODE::SELECT;
  pickBufferValid_ = false;
  brushSize_ = 1;
  pressedButtons_ = Qt::NoButton;
  setMouseTracking(true);
  contextMenu_ = nullptr;

  modelData_.path = modelPath;
  tableData_.gridStep = 10;
  tableData_.lineWidth = 1;

  tableGL_.visible = false;

  axesGL_.visible = false;

  axesData_.detached = true;
}

ViewWidget::~ViewWidget()
{

}

void ViewWidget::initializeGL()
{
  const int glslVer = glslVersionStringToNumber((const char*)glGetString(GL_SHADING_LANGUAGE_VERSION));
  qDebug() << "GLSL decoded: " << glslVer;

  if (glslVer < GLSL_REQUIRED_VERSION)
  {
    qDebug() << "Supported GLSG version (" << glslVer << ") is too low, must be >= " << GLSL_REQUIRED_VERSION;
    return;
  }

  const QString ext = (const char*)glGetString(GL_EXTENSIONS);
  if (!ext.contains(QString("GL_ARB_vertex_array_object")))
  {
    qDebug() << "Failed to initialize OpenGL: GL_ARB_vertex_array_object is required";
    return;
  }
  if (!ext.contains(QString("GL_ARB_explicit_uniform_location")))
  {
    qDebug() << "Failed to initialize OpenGL: GL_ARB_explicit_uniform_location is required";
    return;
  }
  if (!ext.contains(QString("GL_ARB_explicit_attrib_location")))
  {
    qDebug() << "Failed to initialize OpenGL: GL_ARB_explicit_attrib_location is required";
    return;
  }

  //
  // Initialize sahders for model drawing
  //
  if (!modelGL_.programDraw.addShaderFromSourceCode(QOpenGLShader::Vertex, QLatin1String(shaderHeaderSrc) + QLatin1String(shaderModelDrawVertexSrc)))
  {
    qDebug() << "Failed to compile vertex shader\n" << modelGL_.programDraw.log();
    return;
  }

  if (!modelGL_.programDraw.addShaderFromSourceCode(QOpenGLShader::Fragment, QLatin1String(shaderHeaderSrc) + QLatin1String(shaderModelDrawFragmentSrc)))
  {
    qDebug() << "Failed to compile fragment shader\n" << modelGL_.programDraw.log();
    return;
  }

  if (!modelGL_.programDraw.link())
  {
    qDebug() << "Failed to link shader program\n" << modelGL_.programDraw.log();
    return;
  }

  if (!modelGL_.vaoDraw.create())
  {
    qDebug() << "Failed to create model draw VAO";
    return;
  }

  //
  // Initialize shaders fpr model polygon indices offscreen rendering for mouse picking
  //
  if (!modelGL_.programPick.addShaderFromSourceCode(QOpenGLShader::Vertex, QLatin1String(shaderHeaderSrc) + QLatin1String(shaderModelPickVertexSrc)))
  {
    qDebug() << "Failed to compile vertex shader\n" << modelGL_.programPick.log();
    return;
  }

  if (!modelGL_.programPick.addShaderFromSourceCode(QOpenGLShader::Fragment, QLatin1String(shaderHeaderSrc) + QLatin1String(shaderModelPickFragmentSrc)))
  {
    qDebug() << "Failed to compile fragment shader\n" << modelGL_.programPick.log();
    return;
  }

  if (!modelGL_.programPick.link())
  {
    qDebug() << "Failed to link shader program\n" << modelGL_.programPick.log();
    return;
  }

  if (!modelGL_.vaoPick.create())
  {
    qDebug() << "Failed to create model pick VAO";
    return;
  }

  //
  // initialize shaders for table drawing
  //
  if (!tableGL_.program.addShaderFromSourceCode(QOpenGLShader::Vertex, QLatin1String(shaderHeaderSrc) + QLatin1String(shaderTableVertexSrc)))
  {
    qDebug() << "Failed to compile table vertex shader\n" << tableGL_.program.log();
    return;
  }

  if (!tableGL_.program.addShaderFromSourceCode(QOpenGLShader::Fragment, QLatin1String(shaderHeaderSrc) + QLatin1String(shaderTableFragmentSrc)))
  {
    qDebug() << "Failed to compile table fragment shader\n" << tableGL_.program.log();
    return;
  }

  if (!tableGL_.program.link())
  {
    qDebug() << "Failed to link table shader program\n" << tableGL_.program.log();
    return;
  }

  if (!tableGL_.vao.create())
  {
    qDebug() << "Failed to create table VAO";
    return;
  }

  //
  // Initialize shaders for axis drawing
  //
  if (!axesGL_.program.addShaderFromSourceCode(QOpenGLShader::Vertex, QLatin1String(shaderHeaderSrc) + QLatin1String(shaderAxesVertexSrc)))
  {
    qDebug() << "Failed to compile vertex shader\n" << axesGL_.program.log();
    return;
  }

  if (!axesGL_.program.addShaderFromSourceCode(QOpenGLShader::Fragment, QLatin1String(shaderHeaderSrc) + QLatin1String(shaderAxesFragmentSrc)))
  {
    qDebug() << "Failed to compile fragment shader\n" << axesGL_.program.log();
    return;
  }

  if (!axesGL_.program.link())
  {
    qDebug() << "Failed to link shader program\n" << axesGL_.program.log();
    return;
  }

  if (!axesGL_.vao.create())
  {
    qDebug() << "Failed to create axes VAO";
    return;
  }

  glDisable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
  QColor wall(Qt::lightGray);
  glClearColor(wall.redF(), wall.greenF(), wall.blueF(), wall.alphaF());

  gleFunctions.reset(new QOpenGLFunctions_3_2_Core);
  if (!gleFunctions->initializeOpenGLFunctions())
  {
    qDebug() << "Failed to initialize opengl functions";
    return;
  }

  if (!modelData_.path.isEmpty())
    loadSTLFile(modelData_.path);

  return;
}

void ViewWidget::setTableGridStep(int step)
{
  tableData_.gridStep = step;
  createTable();
  update();
}

void ViewWidget::setTableVisible(bool visible)
{
  tableGL_.visible = visible;
  update();
}

void ViewWidget::resizeGL(int w, int h)
{
  if (!w || !h)
    return;

  glViewport(0, 0, w, h);

  pickBufferValid_ = false;
  rePerspective();
}

void ViewWidget::mousePressEvent(QMouseEvent *event)
{
  if (modelData_.vertices.empty())
    return;

  lastMousePos_ = event->pos();
  lastMouseTime_ = std::chrono::steady_clock::now();

  mouseMode_ = MOUSE_MODE::ROTATE;

  pressedButtons_ |= event->buttons();
}

void ViewWidget::mouseMoveEvent(QMouseEvent *event)
{
  if (modelData_.vertices.empty())
    return;

  if (MOUSE_MODE::ROTATE == mouseMode_)
  {
    if (event->buttons() & Qt::LeftButton)
    {
      GLfloat dalpha = (180 * (GLfloat)(event->y() - lastMousePos_.y()) / height()) / 4;
      GLfloat dbeta = (180 * (GLfloat)(event->x() - lastMousePos_.x()) / width()) / 4;
      rMatrix_.rotate(dalpha, OX_);
      rMatrix_.rotate(dbeta, OY_);
      QMatrix4x4 m;
      m.rotate(-dbeta, OY_);
      m.rotate(-dalpha, OX_);
      OZ_ = m * OZ_;
      OY_ = m * OY_;
      OX_ = m * OX_;
      pickBufferValid_ = false;
    }
    else if (event->buttons() & Qt::RightButton)
    {
      GLfloat dy = 0.01f*cameraPosition_.z*(event->y() - lastMousePos_.y());
      GLfloat dx = 0.01f*cameraPosition_.z*(event->x() - lastMousePos_.x());
      tMatrix_.translate(dx, -dy, 0);
      //y axis on screen points down (that mouse Y axis)
      //but on projection - up
      cameraPosition_.x -= dx;
      cameraPosition_.y += dy;
      pickBufferValid_ = false;
    }
    else if (event->buttons() & Qt::MiddleButton)
    {
      GLfloat dgamma = (180 * (GLfloat)(event->y() - lastMousePos_.y()) / height()) / 4;
      rMatrix_.rotate(dgamma, OZ_);
      QMatrix4x4 m;
      m.rotate(-dgamma, OZ_);
      OZ_ = m * OZ_;
      OY_ = m * OY_;
      OX_ = m * OX_;
      pickBufferValid_ = false;
    }
    updateRotationAngles();
    update();

    lastMousePos_ = event->pos();
  }
  else if (MOUSE_MODE::SELECT == mouseMode_)
  {
    const int h = height();
    const int x = event->x();
    const int y = h - 1 - event->y();
    const int i = getPolygonIndex(x, y);
    if (i != lastSelectedPolygon_)
    {
      //qDebug() << "Selection from  " << lastSelectedPolygon_ << " to " << i;
      //qDebug() << "Color " << i * 3 << " of " << modelData_.colors.size();
      if (lastSelectedPolygon_ >= 0)
        setPolygonColor(lastSelectedPolygon_, DefaultColor);
      if (i >= 0)
        setPolygonColor(i, SelectedColor);
      lastSelectedPolygon_ = i;
      update();
    }
  }
  event->accept();
}

void ViewWidget::mouseReleaseEvent(QMouseEvent *event)
{
  bool isLeftHold = event->buttons() & Qt::LeftButton;
  //bool isLeftUnpress = (pressedButtons_ & Qt::LeftButton)
  //                     && !isLeftHold;
  bool isMiddleHold = event->buttons() & Qt::MiddleButton;
  //bool isMiddleUnpress = (pressedButtons_ & Qt::MiddleButton)
  //                       && !isMiddleHold;
  bool isRightHold = event->buttons() & Qt::RightButton;
  bool isRightUnpress = (pressedButtons_ & Qt::RightButton)
                        && !isRightHold;

  //qDebug() << event->buttons();

  std::chrono::steady_clock::time_point t = std::chrono::steady_clock::now();
  bool isClick = std::chrono::duration_cast<std::chrono::milliseconds>(t-lastMouseTime_).count() < 200u;

  if (isRightUnpress && isClick)
    getContextMenu()->popup(this->mapToGlobal(event->pos()));

  if (!isLeftHold & !isMiddleHold & !isRightHold)
    mouseMode_ = MOUSE_MODE::SELECT;

  pressedButtons_ = event->buttons();
  return;
}

void ViewWidget::wheelEvent(QWheelEvent * event)
{
  if (modelData_.vertices.empty())
    return;

  const int delta = event->angleDelta().y();
  const float dz = 0.01f * std::abs(cameraPosition_.z);
  if (delta < 0)
  {
    tMatrix_.translate(0,0,dz);
    cameraPosition_.z -= dz;
    pickBufferValid_ = false;
    rePerspective();
    update();
  }
  else if (delta > 0)
  {
    tMatrix_.translate(0,0,-dz);
    cameraPosition_.z += dz;
    pickBufferValid_ = false;
    rePerspective();
    update();
  }

  event->accept();
  return;
}

void ViewWidget::paintGL()
{
  if (modelData_.vertices.empty())
    return;

  QColor wall(Qt::lightGray);
  glClearColor(wall.redF(), wall.greenF(), wall.blueF(), wall.alphaF());

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  QMatrix4x4 ptrMatrix;
  ptrMatrix = pMatrix_ * tMatrix_ * rMatrix_;
  QVector3D newLightDir;
#if 1
  //if (lightRelativeToCamera) //if true -> using camera light coordinates
  {
    for (int row = 0; row < 3; ++row )
      newLightDir[row] = rMatrix_(0, row) * lightDir_[0] + rMatrix_(1, row) * lightDir_[1] + rMatrix_(2, row) * lightDir_[2];
  }
#else
  //else
    newLightDir = lightDir_;
#endif
  newLightDir /= std::sqrt(newLightDir.x() * newLightDir.x() + newLightDir.y() * newLightDir.y() + newLightDir.z() * newLightDir.z());

  modelGL_.programDraw.bind();
  modelGL_.vaoDraw.bind();

  modelGL_.programDraw.setUniformValue(UNI_DRAW_MVP_LOCATION, ptrMatrix);
  CHECK_GL_ERROR("at setting model MVP");
  modelGL_.programDraw.setUniformValue(UNI_DRAW_LIGHTDIR_LOCATION, newLightDir);
  CHECK_GL_ERROR("at setting model LIGHT");

  glDrawArrays(GL_TRIANGLES, 0, modelData_.vertices.size());
  CHECK_GL_ERROR("at model render");

  //dumpDepth("/home/max/test/viewDepth.png");

  modelGL_.vaoDraw.release();
  modelGL_.programDraw.release();

  if (axesGL_.visible)
  {
    axesGL_.program.bind();
    axesGL_.vao.bind();

    QMatrix4x4 ptrMatrixAxes; // own ptrMatrix
    if (axesData_.detached)
      ptrMatrixAxes = axesData_.ptMatrix * rMatrix_;
    else
      ptrMatrixAxes = ptrMatrix;

    axesGL_.program.setUniformValue(UNI_AXES_MVP_LOCATION, ptrMatrixAxes);
    CHECK_GL_ERROR("at setting table MVP");
    axesGL_.program.setUniformValue(UNI_AXES_LIGHTDIR_LOCATION, newLightDir);
    CHECK_GL_ERROR("at setting table LIGHT");

    glDrawArrays(GL_TRIANGLES, 0, axesData_.vertexCount);
    CHECK_GL_ERROR("at axes render");

    axesGL_.vao.release();
    axesGL_.program.release();
  }

  if (tableGL_.visible)
  {
    tableGL_.program.bind();
    tableGL_.vao.bind();

    tableGL_.program.setUniformValue(UNI_TABLE_MVP_LOCATION, ptrMatrix);
    CHECK_GL_ERROR("at setting table MVP");
    tableGL_.program.setUniformValue(UNI_TABLE_LIGHTDIR_LOCATION, newLightDir);
    CHECK_GL_ERROR("at setting table LIGHT");

    if (tableGL_.texture)
      tableGL_.texture->bind();

    glDrawArrays(GL_TRIANGLES, 0, 6);
    CHECK_GL_ERROR("at table render");

    tableGL_.vao.release();
    tableGL_.program.release();
  }
}

bool ViewWidget::renderIndices()
{
  QOpenGLWidget::makeCurrent();

  if (modelData_.vertices.empty())
    return true;

  if (!indexFrameBuffer_ || (indexFrameBuffer_->width() != this->width()) || (indexFrameBuffer_->height() != this->height()))
  {
    QOpenGLFramebufferObject::Attachment attachment = QOpenGLFramebufferObject::CombinedDepthStencil;
    indexFrameBuffer_.reset(new QOpenGLFramebufferObject(this->size(), attachment));
  }

  if (!indexFrameBuffer_->bind())
  {
    CHECK_GL_ERROR("at setting MVP");
    return false;
  }

  QColor wall(Qt::black);
  glClearColor(wall.redF(), wall.greenF(), wall.blueF(), wall.alphaF());

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  QMatrix4x4 ptrMatrix;
  ptrMatrix = pMatrix_ * tMatrix_ * rMatrix_;

  if (!modelGL_.programPick.bind())
    return false;
  modelGL_.vaoPick.bind();

  modelGL_.programPick.setUniformValue(UNI_PICK_MVP_LOCATION, ptrMatrix);
  CHECK_GL_ERROR("at setting MVP");

  glDrawArrays(GL_TRIANGLES, 0, modelData_.vertices.size());
  CHECK_GL_ERROR("at model render");

  //dumpDepth("/home/max/test/indexDepth.png");

  modelGL_.vaoPick.release();
  modelGL_.programPick.release();
  indexFrameBuffer_->release();
  pickBufferValid_ = true;

  //QImage fboImage(indexFrameBuffer_->toImage());
  //QImage image(fboImage.constBits(), fboImage.width(), fboImage.height(), QImage::Format_ARGB32);
  //image.save("/home/max/test/fb.png");

  return true;
}

int ViewWidget::getPolygonIndex(const int x, const int y)
{
  if (!pickBufferValid_)
  {
    if (!renderIndices())
    {
      qDebug() << "Pick buffer is not updated!!!";
      return 0;
    }
  }

  indexFrameBuffer_->bind();
  Color4b data;
  glReadPixels(x, y, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, &data);
  GLenum errcode = glGetError();
  if (errcode != GL_NO_ERROR)
    qDebug() << "x = " << x << "y =" << y << "w =" << indexFrameBuffer_->width() << "h =" << indexFrameBuffer_->height();

  indexFrameBuffer_->release();
  uint32_t idx = colorToIndex(data);
  //qDebug() << data.r << data.g << data.b << data.a << "->" << idx;
  return (int)idx - 1;
}

bool ViewWidget::loadSTLFile(const QString &path)
{
  if (!readSTL(path, &modelData_.vertices))
    return false;
  modelData_.path = path;

  if (!syncModel())
    return false;

  resetMVP();
  return true;
}

bool ViewWidget::syncModel()
{
  QOpenGLWidget::makeCurrent();
  if (modelGL_.vertices.isCreated())
    modelGL_.vertices.destroy();
  modelGL_.vertices = QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
  modelGL_.vertices.create();
  modelGL_.vertices.setUsagePattern(QOpenGLBuffer::StaticDraw);
  modelGL_.vertices.bind();
  modelGL_.vertices.allocate(modelData_.vertices.size() * 3 * sizeof(float));
  modelGL_.vertices.write(0, modelData_.vertices.data(), modelData_.vertices.size() * 3 * sizeof(float));
  modelGL_.vaoDraw.bind();
  modelGL_.programDraw.enableAttributeArray(ATTR_IN_DRAW_VERTEX_LOCATION);
  modelGL_.programDraw.setAttributeBuffer(ATTR_IN_DRAW_VERTEX_LOCATION, GL_FLOAT, 0, 3, 0);
  modelGL_.vaoDraw.release();
  modelGL_.vaoPick.bind();
  modelGL_.programPick.enableAttributeArray(ATTR_IN_PICK_VERTEX_LOCATION);
  modelGL_.programPick.setAttributeBuffer(ATTR_IN_PICK_VERTEX_LOCATION, GL_FLOAT, 0, 3, 0);
  modelGL_.vaoPick.release();
  modelGL_.vertices.release();

  const size_t triangleCount = modelData_.vertices.size() / 3;

  radius_ = -1;
  std::vector<Point3f> buffer(modelData_.vertices.size());
  for (size_t i = 0; i < triangleCount; ++i)
  {
    const Point3f &v1 = modelData_.vertices[i * 3];
    const Point3f &v2 = modelData_.vertices[i * 3 + 1];
    const Point3f &v3 = modelData_.vertices[i * 3 + 2];

    const Point3f n = normal(v1, v2, v3);
    radius_ = std::max(radius_, v1.x * v1.x + v1.y * v1.y + v1.z * v1.z);

    buffer[i * 3] = n;
    buffer[i * 3 + 1] = n;
    buffer[i * 3 + 2] = n;
  }
  radius_ = std::sqrt(radius_);

  modelGL_.normals = QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
  modelGL_.normals.create();
  modelGL_.normals.setUsagePattern(QOpenGLBuffer::StaticDraw);
  modelGL_.normals.bind();
  modelGL_.normals.allocate(buffer.size() * 3 * sizeof(float));
  modelGL_.normals.write(0, buffer.data(), buffer.size() * sizeof(Point3f));
  modelGL_.vaoDraw.bind();
  modelGL_.programDraw.enableAttributeArray(ATTR_IN_DRAW_NORMAL_LOCATION);
  modelGL_.programDraw.setAttributeBuffer(ATTR_IN_DRAW_NORMAL_LOCATION, GL_FLOAT, 0, 3, 0);
  modelGL_.vaoDraw.release();
  modelGL_.normals.release();

  modelData_.colors.resize(modelData_.vertices.size());
  for (size_t i = 0; i < modelData_.colors.size(); ++i)
    modelData_.colors[i] = DefaultColor;

  modelGL_.colors = QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
  modelGL_.colors.create();
  modelGL_.colors.setUsagePattern(QOpenGLBuffer::StaticDraw);
  modelGL_.colors.bind();
  modelGL_.colors.allocate(modelData_.colors.size() * sizeof(Color4b));
  modelGL_.colors.write(0, modelData_.colors.data(), modelData_.colors.size() * sizeof(Color4b));
  modelGL_.vaoDraw.bind();
  modelGL_.programDraw.enableAttributeArray(ATTR_IN_DRAW_COLOR_LOCATION);
  modelGL_.programDraw.setAttributeBuffer(ATTR_IN_DRAW_COLOR_LOCATION, GL_UNSIGNED_BYTE, 0, 4, 0);
  modelGL_.vaoDraw.release();
  modelGL_.colors.release();

  std::vector<Color4b> indexBuffer(modelData_.vertices.size());
  for (size_t i  = 0; i < indexBuffer.size(); ++i)
    indexBuffer[i] = indexToColor((i / 3) + 1);

  modelGL_.indices = QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
  modelGL_.indices.create();
  modelGL_.indices.setUsagePattern(QOpenGLBuffer::StaticDraw);
  modelGL_.indices.bind();
  modelGL_.indices.allocate(indexBuffer.size() * sizeof(uint32_t));
  modelGL_.indices.write(0, indexBuffer.data(), indexBuffer.size() * sizeof(Color4b));
  modelGL_.vaoPick.bind();
  modelGL_.programPick.enableAttributeArray(ATTR_IN_PICK_INDEX_LOCATION);
  modelGL_.programPick.setAttributeBuffer(ATTR_IN_PICK_INDEX_LOCATION, GL_UNSIGNED_BYTE, 0, 4, 0);
  modelGL_.vaoPick.release();
  //modelGL_.vaoDraw.bind();
  //modelGL_.programDraw.enableAttributeArray(ATTR_IN_DRAW_INDEX_LOCATION);
  //modelGL_.programDraw.setAttributeBuffer(ATTR_IN_DRAW_INDEX_LOCATION, GL_UNSIGNED_BYTE, 0, 4, 0);
  //modelGL_.vaoDraw.release();
  modelGL_.indices.release();

  pickBufferValid_ = false;

  CHECK_GL_ERROR("syncModel");

  if (!createTable())
    return false;

  return true;
}

bool ViewWidget::createTable()
{
  const float tableSize = radius_ * 2;

  Point3f tableVxCoords[6] = {
    Point3f{-tableSize, 0, +tableSize}, Point3f{+tableSize, 0, +tableSize}, Point3f{+tableSize, 0, -tableSize},
    Point3f{+tableSize, 0, -tableSize}, Point3f{-tableSize, 0, -tableSize}, Point3f{-tableSize, 0, +tableSize}
  };

  /*Point2f tableTxCoords[6] = {
    Point2f{0, 1}, Point2f{1, 1}, Point2f{1, 0},
    Point2f{1, 0}, Point2f{0, 0}, Point2f{0, 1}
  };//*/

  float t = tableSize / tableData_.gridStep;
  Point2f tableTxCoords[6] = {
    Point2f{0, t}, Point2f{t, t}, Point2f{t, 0},
    Point2f{t, 0}, Point2f{0, 0}, Point2f{0, t}
  };//*/

  QOpenGLWidget::makeCurrent();
  if (tableGL_.vertices.isCreated())
    tableGL_.vertices.destroy();
  tableGL_.vertices = QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
  tableGL_.vertices.create();
  tableGL_.vertices.setUsagePattern(QOpenGLBuffer::StaticDraw);
  tableGL_.vertices.bind();
  tableGL_.vertices.allocate(6 * 3 * sizeof(float));
  tableGL_.vertices.write(0, tableVxCoords, 6 * 3 * sizeof(float));
  tableGL_.vao.bind();
  tableGL_.program.enableAttributeArray(ATTR_IN_TABLE_VERTEX_LOCATION);
  tableGL_.program.setAttributeBuffer(ATTR_IN_TABLE_VERTEX_LOCATION, GL_FLOAT, 0, 3, 0);
  tableGL_.vao.release();
  tableGL_.vertices.release();

  if (tableGL_.txcoords.isCreated())
    tableGL_.txcoords.destroy();
  tableGL_.txcoords = QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
  tableGL_.txcoords.create();
  tableGL_.txcoords.setUsagePattern(QOpenGLBuffer::StaticDraw);
  tableGL_.txcoords.bind();
  tableGL_.txcoords.allocate(6 * 2 * sizeof(float));
  tableGL_.txcoords.write(0, tableTxCoords, 6 * 2 * sizeof(float));
  tableGL_.vao.bind();
  tableGL_.program.enableAttributeArray(ATTR_IN_TABLE_TXCOORD_LOCATION);
  tableGL_.program.setAttributeBuffer(ATTR_IN_TABLE_TXCOORD_LOCATION, GL_FLOAT, 0, 2, 0);
  tableGL_.vao.release();
  tableGL_.txcoords.release();


  QImage gridCell(16, 16, QImage::Format_RGB888);
  const QRgb lineColor = qRgb(0, 0, 0);
  const QRgb spaceColor = qRgb(128, 128, 128);
  for (int y = 0; y < gridCell.height(); ++y)
  {
    for (int x = 0; x < gridCell.width(); ++x)
    {
      const bool isLine = (0 == x) | (0 == y) | ((gridCell.height() - 1) == y) | ((gridCell.width() - 1) == x);
      gridCell.setPixel(x, y, isLine ? lineColor : spaceColor);
    }
  }

  tableGL_.texture.reset(new QOpenGLTexture(gridCell.mirrored()));

  createAxesArrows();

  return true;
}

bool ViewWidget::createAxesArrows()
{
  axesData_.vertexCount = 0;
  std::vector<Point3f> vertices;
  if (!readSTL(":/assets/XYZ.stl", &vertices))
    return false;

  QOpenGLWidget::makeCurrent();

  if (axesGL_.vertices.isCreated())
    axesGL_.vertices.destroy();
  axesGL_.vertices = QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
  axesGL_.vertices.create();
  axesGL_.vertices.setUsagePattern(QOpenGLBuffer::StaticDraw);
  axesGL_.vertices.bind();
  axesGL_.vertices.allocate(vertices.size() * 3 * sizeof(float));
  axesGL_.vertices.write(0, vertices.data(), vertices.size() * 3 * sizeof(float));
  axesGL_.vao.bind();
  axesGL_.program.enableAttributeArray(ATTR_IN_DRAW_VERTEX_LOCATION);
  axesGL_.program.setAttributeBuffer(ATTR_IN_DRAW_VERTEX_LOCATION, GL_FLOAT, 0, 3, 0);
  axesGL_.vao.release();
  axesGL_.vertices.release();

  const size_t triangleCount = vertices.size() / 3;

  std::vector<Point3f> buffer(vertices.size());
  for (size_t i = 0; i < triangleCount; ++i)
  {
    const Point3f n = normal(vertices[i * 3],
                             vertices[i * 3 + 1],
                             vertices[i * 3 + 2]);

    buffer[i * 3] = n;
    buffer[i * 3 + 1] = n;
    buffer[i * 3 + 2] = n;
  }

  axesGL_.normals = QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
  axesGL_.normals.create();
  axesGL_.normals.setUsagePattern(QOpenGLBuffer::StaticDraw);
  axesGL_.normals.bind();
  axesGL_.normals.allocate(buffer.size() * 3 * sizeof(float));
  axesGL_.normals.write(0, buffer.data(), buffer.size() * sizeof(Point3f));
  axesGL_.vao.bind();
  axesGL_.program.enableAttributeArray(ATTR_IN_AXES_NORMAL_LOCATION);
  axesGL_.program.setAttributeBuffer(ATTR_IN_AXES_NORMAL_LOCATION, GL_FLOAT, 0, 3, 0);
  axesGL_.vao.release();
  axesGL_.normals.release();

  std::vector<Color4b> colors;
  colors.resize(vertices.size());
  for (size_t i = 0; i < colors.size(); ++i)
  {
    uint8_t r = (vertices[i].x > vertices[i].y) && (vertices[i].x > vertices[i].z) ? 255 : 0;
    uint8_t g = (vertices[i].y > vertices[i].x) && (vertices[i].y > vertices[i].z) ? 255 : 0;
    uint8_t b = (vertices[i].z > vertices[i].x) && (vertices[i].z > vertices[i].y) ? 255 : 0;
    colors[i] = Color4b{r, g, b, 255};
  }

  axesGL_.colors = QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
  axesGL_.colors.create();
  axesGL_.colors.setUsagePattern(QOpenGLBuffer::StaticDraw);
  axesGL_.colors.bind();
  axesGL_.colors.allocate(colors.size() * sizeof(Color4b));
  axesGL_.colors.write(0, colors.data(), colors.size() * sizeof(Color4b));
  axesGL_.vao.bind();
  axesGL_.program.enableAttributeArray(ATTR_IN_AXES_COLOR_LOCATION);
  axesGL_.program.setAttributeBuffer(ATTR_IN_AXES_COLOR_LOCATION, GL_UNSIGNED_BYTE, 0, 4, 0);
  axesGL_.vao.release();
  axesGL_.colors.release();


  QMatrix4x4 p, t;
  p.perspective(60, (float)width()/(float)height(), 300.0f, 500.0f);
  t.translate(0, 0, -400.0f);
  axesData_.ptMatrix = p * t;

  axesData_.vertexCount = vertices.size();
  axesGL_.visible = vertices.size() > 0;
  return true;
}

void ViewWidget::updateRotationAngles()
{
  if (modelData_.vertices.empty())
    return;

  modelOrientation_ = extractRotationAngles(rMatrix_, true);
  return;
}

void ViewWidget::reRotate()
{
  QMatrix4x4 m;
  m.setToIdentity();
  OX_ = {1, 0, 0};
  OY_ = {0, 1, 0};
  OZ_ = {0, 0, 1};
  rMatrix_.setToIdentity();
  rMatrix_.rotate(modelOrientation_.a, OX_);
  rMatrix_.rotate(modelOrientation_.b, OY_);
  rMatrix_.rotate(modelOrientation_.g, OZ_);
  m.rotate(-modelOrientation_.g, OZ_);
  m.rotate(-modelOrientation_.b, OY_);
  m.rotate(-modelOrientation_.a, OX_);
  OZ_ = m * OZ_;
  OY_ = m * OY_;
  OX_ = m * OX_;
  pickBufferValid_ = false;
  return;
}

void ViewWidget::reTranslate()
{
  tMatrix_.setToIdentity();
  tMatrix_.translate(-cameraPosition_.x, -cameraPosition_.y, -cameraPosition_.z);
  pickBufferValid_ = false;
  return;
}

void ViewWidget::rePerspective()
{
  //constexpr int photoWidth = 320;
  //constexpr int photoHeight = 256;
  //constexpr float focLen = 25.00f;
  //constexpr float pixDim = 0.014f;

  //const int pw = photoWidth;
  //const int ph = photoHeight;
  //const float ar = (float)photoWidth/(float)photoHeight;
  const float ar = (float)width() / (float)height();

  float nearPlane = std::abs(cameraPosition_.z) - radius_ * 2.0f * 1.42f;
  float farPlane = nearPlane + radius_ * 4.0f * 1.42f;

  //qDebug() << nearPlane << ".." << farPlane;

  nearPlane = std::max(0.01f * radius_, nearPlane);
  farPlane = std::max(radius_, farPlane);

  pMatrix_.setToIdentity();
  pMatrix_.perspective(60.0, ar, nearPlane, farPlane);

  //pMatrix_(0, 0) = (focLen/pixDim)/(pw/2); //(foc_len_x/pix_dim_x)
  //pMatrix_(1, 1) = (focLen/pixDim)/(ph/2);
  pickBufferValid_ = false;

  return;
}

Point3f ViewWidget::restorePerspectivePoint(const int x, const int y, const float depth) const
{
  const float w = width();
  const float h = height();

  Point3f p3;
  // 1. Unproject
  // map depth to (near..far) plane
  const float zndc = 2 * depth - 1; //- distance btween point and camera
  // point position on OZ (after rotation)
  // get point displacement agaist projection ray
  p3.z = (zndc * pMatrix_(3,3) - pMatrix_(2,3)) / (zndc * pMatrix_(3,2) - pMatrix_(2,2));
  p3.x = (x - w / 2);
  p3.y = (y - h / 2);
  // perspective unprojection (with camera in mind)
  p3.x *= 2 / (w * pMatrix_(0, 0)) * p3.z;
  p3.y *= 2 / (h * pMatrix_(1, 1)) * p3.z;

  // 2. Untranslate
  p3.x = p3.x + cameraPosition_.x;
  p3.y = p3.y + cameraPosition_.y;
  p3.z = -p3.z + cameraPosition_.z;

  // 3.Unrotate
  QMatrix4x4 m;
  m.setToIdentity();
  m.rotate(-modelOrientation_.g, OZ_);
  m.rotate(-modelOrientation_.b, OY_);
  m.rotate(-modelOrientation_.a, OX_);
  QVector4D position(p3.x, p3.y, p3.z, 0);

  position = m * position;
  p3.x = position.x();
  p3.y = position.y();
  p3.z = position.z();

  return p3;
}

void ViewWidget::setPolygonColor(const size_t ind, const Color4b &color)
{
  modelGL_.colors.bind();
  modelData_.colors[ind * 3] = color;
  modelData_.colors[ind * 3 + 1] = color;
  modelData_.colors[ind * 3 + 2] = color;
  modelGL_.colors.write(ind * 4 * 3, &modelData_.colors[ind * 3], 4 * 3);
  CHECK_GL_ERROR("write color");
  modelGL_.colors.release();

  return;
}

QMenu* ViewWidget::getContextMenu()
{
  if (!contextMenu_)
  {
    contextMenu_ = new QMenu("title", this);
    contextMenu_->addAction("Lay on polygon", this, &ViewWidget::layOnPolygon);
    contextMenu_->addAction("Turn upside down", this, &ViewWidget::turnUpsideDown);
    contextMenu_->addAction("Reset view", this, &ViewWidget::resetMVP);
  }

  return contextMenu_;
}

void ViewWidget::layOnPolygon()
{
  if (lastSelectedPolygon_ >= 0)
  {
    const Point3f c1 = (modelData_.vertices[lastSelectedPolygon_ * 3]
                        + modelData_.vertices[lastSelectedPolygon_ * 3 + 1]
                        + modelData_.vertices[lastSelectedPolygon_ * 3 + 2]) / 3;
    const Point3f c2 = {0, 0, 0};
    // https://stackoverflow.com/questions/10706708/find-x-y-z-rotation-between-two-normal-vectors
    // currently not working
    const Vector3f n1 = normal(modelData_.vertices[lastSelectedPolygon_ * 3],
                               modelData_.vertices[lastSelectedPolygon_ * 3 + 1],
                               modelData_.vertices[lastSelectedPolygon_ * 3 + 2]);
    const Vector3f n2{0, -1, 0};

    Vector3f axis = cross(n1, n2);
    normalize(&axis);

    QMatrix4x4 m;
    m.setToIdentity();
    bool mustTurnUpsideDown= false;
    if (!std::isnan(axis.x) & !std::isnan(axis.y) & !std::isnan(axis.z))
    {
      double angle = std::acos(dot(n1, n2));

      Angle3f rot = toEuler(axis.x, axis.y, axis.z, angle);


      // see special order: https://www.euclideanspace.com/maths/standards/index.htm
      m.rotate(rot.b * 180 / M_PI, QVector3D{0, 1, 0});
      m.rotate(rot.g * 180 / M_PI, QVector3D{0, 0, 1});
      m.rotate(rot.a * 180 / M_PI, QVector3D{1, 0, 0});
    }
    // else // incldues the case, when n1 == n2 and n1 == -n2
    else if (dot(n1, n2) < 0)
      mustTurnUpsideDown = true;

    m.translate(c2.x - c1.x, c2.y - c1.y, c2.z - c1.z);

    for (size_t i = 0; i < modelData_.vertices.size(); ++i)
    {
      QVector3D v(modelData_.vertices[i].x, modelData_.vertices[i].y, modelData_.vertices[i].z);
      v = m * v;
      modelData_.vertices[i] = Point3f{v.x(), v.y(), v.z()};
    }

    if (mustTurnUpsideDown)
      turnUpsideDown(); // does syncMode(); update();
    else
    {
      syncModel();
      update();
    }
  }
}

void ViewWidget::turnUpsideDown()
{
  for (size_t i = 0; i < modelData_.vertices.size(); i += 3)
  {
    // if only apply -1 to Y, then vertice order in polygon becomes inverted (CW<->CCW)
    //    have to swap 1st vertex with 3rd
    Point3f buf = modelData_.vertices[i];
    modelData_.vertices[i] = Point3f{modelData_.vertices[i + 2].x, -modelData_.vertices[i + 2].y, modelData_.vertices[i + 2].z};
    modelData_.vertices[i + 1].y = -modelData_.vertices[i + 1].y;
    modelData_.vertices[i + 2] = Point3f{buf.x, -buf.y, buf.z};
  }
  syncModel();
  update();
}

void ViewWidget::resetMVP()
{
  modelOrientation_ = {0, 0, 0};
  cameraPosition_ = {0, 0, 3 * radius_};

  rePerspective();
  reTranslate();
  reRotate();
  update();
}

void ViewWidget::dumpDepth(const QString &path)
{
  const int w = width();
  const int h = height();
  std::vector<float> depthBuf(w * h);
  glReadPixels(0, 0, w, h, GL_DEPTH_COMPONENT, GL_FLOAT, depthBuf.data());
  float mind = std::numeric_limits<float>::max();
  float maxd = std::numeric_limits<float>::lowest();
  for (size_t i = 0; i < depthBuf.size(); ++i)
  {
    mind = std::min(mind, depthBuf[i]);
    maxd = std::max(maxd, depthBuf[i]);
  }
  QImage img(w, h, QImage::Format_RGB32);
  for (int y = 0; y < h; ++y)
    for (int x = 0; x < w; ++x)
    {
      int v = 255 * (depthBuf[y * w + x] - mind) / (maxd - mind);
      img.setPixel(x, y, qRgb(v,v,v));
    }
  img.save(path);
}
