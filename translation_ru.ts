<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>DataWidget</name>
    <message>
        <location filename="DataWidget.cpp" line="46"/>
        <source>Print</source>
        <translation>Печать</translation>
    </message>
    <message>
        <location filename="DataWidget.cpp" line="49"/>
        <source>Nozzle </source>
        <translation>Сопло </translation>
    </message>
    <message>
        <location filename="DataWidget.cpp" line="50"/>
        <source>Plastic
Feed</source>
        <translation>Подача
Пластика</translation>
    </message>
    <message>
        <location filename="DataWidget.cpp" line="51"/>
        <source>Fiber
Feed</source>
        <translation>Подачав
Волокна</translation>
    </message>
    <message>
        <location filename="DataWidget.cpp" line="52"/>
        <source>Filament</source>
        <translation>Филамент</translation>
    </message>
    <message>
        <location filename="DataWidget.cpp" line="53"/>
        <source>Reinforcing
Fiber</source>
        <translation>Армирующее
Волокно</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="MainWindow.cpp" line="30"/>
        <source>Slice Layers</source>
        <translation>Нарезка Слоев</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="32"/>
        <source>Generate G-Code</source>
        <translation>Генерация G-Кода</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="37"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="38"/>
        <source>New Project</source>
        <translation>Новый Проект</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="39"/>
        <source>Load Project</source>
        <translation>Загрузить Проект</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="40"/>
        <source>Save Project</source>
        <translation>Сохранить Проект</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="41"/>
        <source>Import STL</source>
        <translation>Импорт STL</translation>
    </message>
</context>
<context>
    <name>OptionsFilamentWidget</name>
    <message>
        <location filename="widgets/OptionsFilamentWidget.cpp" line="42"/>
        <source>Parameter 1</source>
        <translation>Параметр 1</translation>
    </message>
    <message>
        <location filename="widgets/OptionsFilamentWidget.cpp" line="47"/>
        <source>Parameter 2</source>
        <translation>Параметр 2</translation>
    </message>
</context>
<context>
    <name>OptionsPrintWidget</name>
    <message>
        <location filename="widgets/OptionsPrintWidget.cpp" line="138"/>
        <source>Layer thickness (mm)</source>
        <translation>Толщина слоя печати (мм)</translation>
    </message>
    <message>
        <location filename="widgets/OptionsPrintWidget.cpp" line="139"/>
        <source>1st layer thickness (mm)</source>
        <translation>Толщина первого слоя (мм)</translation>
    </message>
    <message>
        <location filename="widgets/OptionsPrintWidget.cpp" line="140"/>
        <source>Outer perimeters count</source>
        <translation>Число внешних периметров</translation>
    </message>
    <message>
        <location filename="widgets/OptionsPrintWidget.cpp" line="141"/>
        <source>Inner perimeters count</source>
        <translation>Число внутренних периметров</translation>
    </message>
    <message>
        <location filename="widgets/OptionsPrintWidget.cpp" line="142"/>
        <source>Extruder thickness (mm)</source>
        <translation>Толщина экструдера (мм)</translation>
    </message>
    <message>
        <location filename="widgets/OptionsPrintWidget.cpp" line="143"/>
        <source>Reinforcing</source>
        <translation>Армирование</translation>
    </message>
    <message>
        <location filename="widgets/OptionsPrintWidget.cpp" line="144"/>
        <source>Reinforcing layers</source>
        <translation>Армированные слои</translation>
    </message>
    <message>
        <location filename="widgets/OptionsPrintWidget.cpp" line="147"/>
        <source>None</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="widgets/OptionsPrintWidget.cpp" line="147"/>
        <source>All</source>
        <translation>Все</translation>
    </message>
    <message>
        <location filename="widgets/OptionsPrintWidget.cpp" line="147"/>
        <source>Selected</source>
        <translation>Выбранные</translation>
    </message>
    <message>
        <location filename="widgets/OptionsPrintWidget.cpp" line="149"/>
        <source>Reinforcing volume</source>
        <translation>Армирование объема</translation>
    </message>
    <message>
        <location filename="widgets/OptionsPrintWidget.cpp" line="152"/>
        <source>Inner</source>
        <translation>Внутренний</translation>
    </message>
    <message>
        <location filename="widgets/OptionsPrintWidget.cpp" line="152"/>
        <source>Outer</source>
        <translation>Внешний</translation>
    </message>
    <message>
        <location filename="widgets/OptionsPrintWidget.cpp" line="152"/>
        <source>Whole</source>
        <translation>Весь</translation>
    </message>
    <message>
        <location filename="widgets/OptionsPrintWidget.cpp" line="154"/>
        <source>Reinforcing type</source>
        <translation>Типа армирования</translation>
    </message>
    <message>
        <location filename="widgets/OptionsPrintWidget.cpp" line="159"/>
        <source>Reinforcing volume type</source>
        <translation>Тип объемного армирования</translation>
    </message>
    <message>
        <location filename="widgets/OptionsPrintWidget.cpp" line="164"/>
        <source>Filling</source>
        <translation>Заполнение</translation>
    </message>
    <message>
        <location filename="widgets/OptionsPrintWidget.cpp" line="165"/>
        <source>Filling density (%)</source>
        <translation>Плотность заполнения (%)</translation>
    </message>
    <message>
        <location filename="widgets/OptionsPrintWidget.cpp" line="166"/>
        <source>Dense layers above</source>
        <translation>Кол-во плотных слоев сверху</translation>
    </message>
    <message>
        <location filename="widgets/OptionsPrintWidget.cpp" line="167"/>
        <source>Dense layers below</source>
        <translation>Кол-во плотных слоев снизу</translation>
    </message>
    <message>
        <location filename="widgets/OptionsPrintWidget.cpp" line="168"/>
        <source>Plinth</source>
        <translation>Плинтус</translation>
    </message>
    <message>
        <location filename="widgets/OptionsPrintWidget.cpp" line="169"/>
        <source>Edge</source>
        <translation>Край</translation>
    </message>
    <message>
        <location filename="widgets/OptionsPrintWidget.cpp" line="170"/>
        <source>Support</source>
        <translation>Опора</translation>
    </message>
</context>
<context>
    <name>OptionsReinforcingFiberWidget</name>
    <message>
        <location filename="widgets/OptionsReinforcingFiberWidget.cpp" line="36"/>
        <source>Parameter 1</source>
        <translation>Параметр 1</translation>
    </message>
</context>
</TS>
