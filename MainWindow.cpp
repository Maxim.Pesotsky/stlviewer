#include "MainWindow.h"

#include <QtGlobal>
#include <QTextStream>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QCheckBox>
#include <QSpinBox>
#include <QMenuBar>
#include <QWidget>
#include <QMenu>
#include <QFile>

#include "ViewWidget.h"
#include "DataWidget.h"
#include "types/Config.h"

MainWindow::MainWindow(const QString &modelPath, QWidget *parent) : QMainWindow(parent)
{
  QWidget *pwgtCentral = new QWidget(this);
  this->setCentralWidget(pwgtCentral);
  QVBoxLayout *plytCentral = new QVBoxLayout(pwgtCentral);
  {
    QHBoxLayout *plytViewData = new QHBoxLayout;
    {
      QVBoxLayout *plytView = new QVBoxLayout;
      {
        plytView->addWidget(viewWidget_ = new ViewWidget(modelPath, pwgtCentral), 3);
        viewWidget_->setMinimumWidth(320);

        QHBoxLayout *plytViewOptions = new QHBoxLayout(pwgtCentral);
        {
          QCheckBox *pchkDisplayTable = new QCheckBox(tr("Display table"), pwgtCentral);
          pchkDisplayTable->setChecked(true);
          viewWidget_->setTableVisible(pchkDisplayTable->isChecked());
          connect(pchkDisplayTable, &QCheckBox::toggled, viewWidget_, &ViewWidget::setTableVisible);
          plytViewOptions->addWidget(pchkDisplayTable);

          QSpinBox *pspnTableGridStep = new QSpinBox(pwgtCentral);
          pspnTableGridStep->setMinimum(0);
          pspnTableGridStep->setMaximum(10000);
          pspnTableGridStep->setValue(10);
          connect(pspnTableGridStep, qOverload<int>(&QSpinBox::valueChanged), viewWidget_, &ViewWidget::setTableGridStep);
          plytViewOptions->addWidget(pspnTableGridStep);
        }
        plytView->addLayout(plytViewOptions);
      }
      plytViewData->addLayout(plytView, 9);

      plytViewData->addWidget(dataWidget_ = new DataWidget(pwgtCentral), 1);
    }
    plytCentral->addLayout(plytViewData);

    QHBoxLayout *plytProcessButtons = new QHBoxLayout;
    QPushButton *pbtnSliceLayers = new QPushButton(tr("Slice Layers"), pwgtCentral);
    plytProcessButtons->addWidget(pbtnSliceLayers);
    QPushButton *pbtngenerateGCode = new QPushButton(tr("Generate G-Code"), pwgtCentral);
    plytProcessButtons->addWidget(pbtngenerateGCode);
    plytCentral->addLayout(plytProcessButtons);
  }

  QMenu *pmnuFile = this->menuBar()->addMenu(tr("File"));
  pmnuFile->addAction(tr("New Project"), this, &MainWindow::newProject);
  pmnuFile->addAction(tr("Load Project"), this, &MainWindow::loadProject);
  pmnuFile->addAction(tr("Save Project"), this, &MainWindow::saveProject);
  pmnuFile->addAction(tr("Import STL"), this, &MainWindow::importSTL);

  newProject();
}

MainWindow::~MainWindow()
{

}

void MainWindow::importSTL()
{
  QString path = QFileDialog::getOpenFileName(this, QLatin1String("CAPTION"), "/media/max/Data0/CTT/", QLatin1String("*.stl"));
  if (!path.isEmpty())
    viewWidget_->loadSTLFile(path);
}

void MainWindow::newProject()
{
  if (!data_)
    data_.reset(new Options());

  dataWidget_->readFrom(*data_);
}

void MainWindow::loadProject()
{
  QString path = QFileDialog::getOpenFileName(this, QLatin1String("CAPTION"), "/home/max/test/", QLatin1String("*.xml"));
  if (path.isEmpty())
    return;

  QFile file(path);
  if (!file.open(QFile::ReadOnly | QFile::Text))
    return;
  if (file.size() > 1024 * 1024)
    return;

  QTextStream in(&file);
  in.setCodec("UTF-8");
  QString txt = in.readAll();
  file.close();

  if (!data_)
    data_.reset(new Options);
  readXml(txt, data_.get());

  dataWidget_->readFrom(*data_);
}

void MainWindow::saveProject()
{
  QString path = QFileDialog::getSaveFileName(this, "CAPTION", "/home/max/test/", QLatin1String("*.stl"));
  if (path.isEmpty())
    return;

  dataWidget_->writeTo(data_.get());

  QString buf;
  writeXml(*data_, &buf);

  QFile file(path);
  if (!file.open(QFile::WriteOnly | QFile::Text))
    return;

  file.write(buf.toUtf8());
}




