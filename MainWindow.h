#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <memory>

#include <QMainWindow>

class ViewWidget;
class DataWidget;

class Options;

class MainWindow : public QMainWindow
{
  Q_OBJECT
public:
  explicit MainWindow(const QString &modelPath = QLatin1Literal(""), QWidget *parent = nullptr);
  ~MainWindow();

public slots:
  void importSTL();
  void loadProject();
  void saveProject();
  void newProject();

signals:

private:
  ViewWidget *viewWidget_;
  DataWidget *dataWidget_;
  std::unique_ptr<Options> data_;
};

#endif // MAINWINDOW_H
